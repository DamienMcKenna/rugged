---
title: "Rugged"

---

Rugged - A TUF Server
=====================

**Rugged** is a server-side implementation of [The Update Framework](/reference/tuf/) (TUF). TUF aims to secure software supply chains.

---

That is, by implementing TUF, package managers (eg. Composer, Pip) can verify that the packages they download and deploy have not been tampered with. However, to do this verification, the repository that hosts the packages (eg. Packagist, PyPi) needs to generate cryptographic signatures of the files they serve.

---

That's where **Rugged** comes in.

**Rugged** aims to make generating those signatures relatively simple, and very, *very* robust.

---

Confused? That's understandable. This stuff is *complicated*. Check out [TUF for Humans](/background/tuf_for_humans) for an introduction to the subject.

Also, it may be worthwhile to check out the **[Glossary of Terms](reference/glossary)** used by this project.
