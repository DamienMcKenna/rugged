---
title: History of the Rugged Project
menuTitle: History
weight: 10

---

Rugged started out as a project by the Drupal Association in support of their [Automatic Updates Initiative](). It sought to secure the software supply chain for allow Drupal sites to securely automatically update themselves.

TUF is [#2 of 4 components highlighted](https://www.drupal.org/node/2940731) as required by the Automatic Updates Initiative.

> **Initiative Overview**
>
> The goal of the Automatic Updates Initiative is to provide safe, secure automatic updates for Drupal sites. This is an essential strategic initiative for Drupal 9 core for two reasons:
>
> * Manually updating sites is difficult, time-consuming, and expensive, and the difficulty of updating Drupal is repeatedly raised as a top concern for using Drupal.
>
> * Delays between the release of a security advisory and the deployment of the corresponding Drupal update result in a time window where sites are vulnerable to exploit. Many sites do not apply updates for 2-4 weeks after the release of a security advisory, and site owners who want to ensure the security of their site need to have site maintainers available during the 1200 EST security release window (which is in the middle of the night for much of the Eastern hemisphere).
>
>Both these factors increase Drupal's cost of ownership and decrease user confidence in the platform.
>

So the project project was to develop the **TUF server architecture and components** which would sign and serve relevant metadata for distribution of Drupal modules (both core and contrib).

The solution also had to work in concert with the [composer plugin](/package-signing/tuf/background/roadmap/discovery/composer_plugin/) which is in development, integrating the [PHP-TUF client library](/package-signing/tuf/background/roadmap/discovery/composer_plugin/php-tuf_library).

For more background, see: [Automatic Updates Initiative meetings](https://www.drupal.org/project/issues/automatic_updates?text=&status=All&priorities=All&categories=All&version=All&component=Meetings&order=created&sort=desc)
