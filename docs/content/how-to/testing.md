---
title: How to test with Behat
---

To only run tests with the `@wip` tag, run:

```
ddev exec bin/behat --tags=wip
```
