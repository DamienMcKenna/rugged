---
title: Development
weight: 5

---

This section covers hands-on example of development tasks.

See the [Architecture](/architecture) section for a broader discussion of the
applications underlying components, and how they fit together.
