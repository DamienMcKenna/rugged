---
title: Satis

---

We use [Satis](https://github.com/composer/satis) to generate a local composer repository for development and testing purposes.

## How to install Satis

`make satis`

## How to update Satis configuration files

See examples in `features/satif.feature`

## How to generate Satis artifacts

See examples in `features/satif.feature`

## How to serve Satis artifacts

See examples in `features/satif.feature`
