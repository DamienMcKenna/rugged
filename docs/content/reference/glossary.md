---
title: Glossary
weight: 100

---

## Glossary

Drumkit
: Drumkit is a collection of scripts and templates designed to standardize and integrate some of the most frequent tasks we encounter in the local development of Drupal sites.

make / GNU Make
: Make is a build automation tool that automatically builds executable programs and libraries from source code by reading files called *Makefiles* which specify how to derive the target program.

DDEV
: DDEV provides pre-configured local development environments powered by Docker containers.

Behat
: Behat is a test framework for behaviour-driven development.

Git
: Git is a version control system for project development code.

Satis
: Satis is a static Composer repository generator.

TUF
: TUF (The Update Framework) is a framework for securing software update systems.
