---
title: Architecture
weight: 10

---

Rugged is composed of:
- a command-line (CLI) tool, `rugged`; and
- a set of worker daemons, each fulfilling a TUF role.

The interactions between these components is illustrated in this sequence diagram of a simplified [package release workflow](package_release).

## Commands

Rugged commands are triggered by TUF administrators directly, or via the packaging pipeline. These will include:

{{% children page="Commands" showhidden="true" startdepth="1" depth=999 %}}

Commands are implemented using the [Click package](/package-signing/tuf/reference/components/click/) and the [shared TUF library](/package-signing/tuf/background/roadmap/proposed/architecture/library/).

## Workers

Workers are services running continuously to provide the day-to-day signing operations of the TUF system.

Workers are implemented using the [Celery distributed task queue](/package-signing/tuf/reference/components/celery/).

They include:

- The **Targets Worker** is responsible for updating the Targets' role metadata with target file hashes, and writing said metadata (`targets.json`).
    - In a future release, this worker may delegate tasks to additional Targets workers, to:
        - distribute workload (for performance); and/or
        - provide additional isolated signing keys (for security).
- The **Snapshot Worker** is responsible for updating and writing the Snapshot role metadata (`snapshot.json`).
- The **Timestamp Worker** is be responsible for updating and writing the Timestamp role metadata (`timestamp.json`).
- The **Root Worker** is be responsible for updating and writing the Root role metadata (`root.json`).
    - **N.B.** The Root Worker is not present during regular operations of the Rugged system. It is spun up only for the performance of administrative tasks, such as:
        - initializing a new TUF repository,
        - generating keypairs,
        - rotating keypairs,
        - etc.


