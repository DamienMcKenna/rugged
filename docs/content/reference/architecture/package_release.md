---
title: Package release workflow
menuTitle: Packaging Workflow
weight: 20

---

The diagram below illustrates a generic package release workflow with Rugged TUF signing integrated.

Examples:
* [`packages.json`](/package-signing/tuf/examples/packages.drupal.org/8/packages.json)
* [`<PACKAGE>.json`](/package-signing/tuf/examples/packages.drupal.org/files/packages/8/p2/drupal/config_enforce.json)
* [`<PACKAGE>-<VERSION>.zip`](/package-signing/tuf/examples/ftp.drupal.org/files/projects/config_enforce-1.0.0-rc2.zip)
* [`targets.json`](/package-signing/tuf/examples/tuf/metadata/targets.json)
* [`snapshot.json`](/package-signing/tuf/examples/tuf/metadata/snapshot.json)
* [`timestamp.json`](/package-signing/tuf/examples/tuf/metadata/timestamp.json)

{{< mermaid >}}

sequenceDiagram

    participant PP as Packaging<br />Pipeline
    participant FS as Shared<br />Filesystem(s)
    participant TUF as Rugged<br />CLI
    participant TARGETS as Targets<br />Worker
    participant SNAPSHOT as Snapshot<br />Worker
    participant TIMESTAMP as Timestamp<br />Worker

    autonumber
    activate PP
    PP->>PP: Clone code
    rect rgba(0, 255, 255, .1)
        note left of FS: &lt;PACKAGE&gt;-&lt;VERSION&gt;.zip
        PP->>PP: Build package
        PP->>FS: Upload package
        activate FS
    end
    rect rgba(0, 255, 0, .1)
        note left of FS: &lt;PACKAGE&gt;.json
        PP->>PP: Generate package<br />metadata
        PP->>FS: Upload package metadata
    end

    PP->>TUF: `rugged add-targets foo/foo.zip foo/package.json [...]`
    activate TUF
    rect rgba(0, 0, 255, .1)
        note right of FS: TUF signing

        TUF->>TARGETS: list of target files to sign
        activate TARGETS
        loop For each target file
            FS-->>TARGETS: Read target file
            TARGETS->>TARGETS: Update Targets metadata<br />(with signature of target file)
        end
        TARGETS->>TARGETS: Sign Targets metadata
        TARGETS-->>FS: Write updated Targets metadata
        note left of TUF: targets.json
        TARGETS->>TUF: Return status message
        deactivate TARGETS
        TUF->>PP: Print status message

        TUF->>SNAPSHOT: Trigger Snapshot update
        activate SNAPSHOT
        SNAPSHOT->>SNAPSHOT: Update and sign<br />Snapshot metadata
        SNAPSHOT-->>FS: Write updated Snapshot metadata
        note left of TUF: snapshot.json
        SNAPSHOT->>TUF: Return status message
        deactivate SNAPSHOT
        TUF->>PP: Print status message

        TUF->>TIMESTAMP: Trigger Timestamp update
        activate TIMESTAMP
        TIMESTAMP->>TIMESTAMP: Update and sign<br />Timestamp metadata
        TIMESTAMP-->>FS: Write updated Timestamp metadata
        deactivate FS
        note left of TUF: timestamp.json
        TIMESTAMP->>TUF: Return status message
        deactivate TIMESTAMP
        TUF->>PP: Print status message
    end
    deactivate TUF
    deactivate PP

{{< /mermaid >}}
