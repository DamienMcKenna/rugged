---
title: "Feature: Signing keys are isolated from one anothe in separate environments."
hidden: True
---

#### Test results for [`features/security/isolated_keys.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/security/isolated_keys.feature)
Running `behat features/security/isolated_keys.feature` results in:

```gherkin
@rugged @security @isolated-keys
Feature: Signing keys are isolated from one anothe in separate environments.
  In order to keep a TUF systems secure,
  As a TUF administrator
  I need to ensure that signing keys are kept in isolated environments.

  Background:
    Given I rebuild fixtures
    And I run "sudo cp features/fixtures/no_host_headers.yaml /var/rugged/.config/rugged/config.yaml"

  Scenario: Each worker only has access to the key(s) needed for its role.
    When I run "sudo -u rugged rugged status --worker=root-worker"
    Then I should get:
      """
      Role       Capability
      ---------  ------------
      targets    Signing
      snapshot   Signing
      timestamp  Signing
      root       Signing
      
      Key name    Role       Key type(s)      Scheme    Path
      ----------  ---------  ---------------  --------  --------------------------------------------
      targets     targets    public, private  ed25519   /var/rugged/signing_keys/targets/targets
      snapshot    snapshot   public, private  ed25519   /var/rugged/signing_keys/snapshot/snapshot
      timestamp   timestamp  public, private  ed25519   /var/rugged/signing_keys/timestamp/timestamp
      root        root       public, private  ed25519   /var/rugged/signing_keys/root/root
      root1       root       public, private  ed25519   /var/rugged/signing_keys/root/root1
      """
    When I run "sudo -u rugged rugged status --worker=timestamp-worker"
    Then I should get:
      """
      targets    Verification
      snapshot   Verification
      timestamp  Signing
      root       Verification
      
      targets     targets    public           ed25519   /var/rugged/verification_keys/targets.pub
      snapshot    snapshot   public           ed25519   /var/rugged/verification_keys/snapshot.pub
      timestamp   timestamp  public, private  ed25519   /var/rugged/signing_keys/timestamp/timestamp
      root        root       public           ed25519   /var/rugged/verification_keys/root.pub
      root1       root       public           ed25519   /var/rugged/verification_keys/root1.pub
      """
    And I should not get:
      """
      targets    Signing
      snapshot   Signing
      root       Signing
      """
    When I run "sudo -u rugged rugged status --worker=snapshot-worker"
    Then I should get:
      """
      targets    Verification
      snapshot   Signing
      timestamp  Verification
      root       Verification
      
      targets     targets    public           ed25519   /var/rugged/verification_keys/targets.pub
      snapshot    snapshot   public, private  ed25519   /var/rugged/signing_keys/snapshot/snapshot
      timestamp   timestamp  public           ed25519   /var/rugged/verification_keys/timestamp.pub
      root        root       public           ed25519   /var/rugged/verification_keys/root.pub
      root1       root       public           ed25519   /var/rugged/verification_keys/root1.pub
      """
    And I should not get:
      """
      targets    Signing
      timestamp  Signing
      root       Signing
      """
    When I run "sudo -u rugged rugged status --worker=targets-worker"
    Then I should get:
      """
      targets    Signing
      snapshot   Verification
      timestamp  Verification
      root       Verification
      
      targets     targets    public, private  ed25519   /var/rugged/signing_keys/targets/targets
      snapshot    snapshot   public           ed25519   /var/rugged/verification_keys/snapshot.pub
      timestamp   timestamp  public           ed25519   /var/rugged/verification_keys/timestamp.pub
      root        root       public           ed25519   /var/rugged/verification_keys/root.pub
      root1       root       public           ed25519   /var/rugged/verification_keys/root1.pub
      """
    And I should not get:
      """
      snapshot   Signing
      timestamp  Signing
      root       Signing
      """

1 scenario (1 passed)
13 steps (13 passed)
```
