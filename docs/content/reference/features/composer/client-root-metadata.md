---
title: "Feature: The TUF Composer plugin requires initial TUF root metadata."
hidden: True
---

#### Test results for [`features/composer/client-root-metadata.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/composer/client-root-metadata.feature)
Running `behat features/composer/client-root-metadata.feature` results in:

```gherkin
@composer-plugin @php-tuf @satis @tuf @rugged @client-root-metadata
Feature: The TUF Composer plugin requires initial TUF root metadata.
  In order to securely build PHP codebases
  As a site builder
  I need Composer to have initial TUF root metadata.

  Background:
    Given I rebuild fixtures
    And I register the Rugged repo with Composer
    And I use the "rugged-composer.json" Composer file
    And I am in the "d9-site" directory
    And I run "composer install -vv"
    And I run "rm -rf web/modules/contrib/token/"

  Scenario: When repo TUF root metadata is present, Composer TUF plugin does not emit an error.
    When I try to run "composer install -vv"
    Then I should get:
      """
      Authenticity of packages from http://packages.ddev.site will be verified by TUF.
      """
    Then I should not get:
      """
      No TUF root metadata was found for repository http://packages.ddev.site.
      """

  Scenario: When repo TUF root metadata is present, Composer TUF plugin should download packages.
    When I try to run "composer install -vv"
    Then I should get:
      """
      Authenticity of packages from http://packages.ddev.site will be verified by TUF.
      """
    And the following files should exist:
      """
      web/modules/contrib/token/token.info.yml
      """

2 scenarios (2 passed)
18 steps (18 passed)
```
