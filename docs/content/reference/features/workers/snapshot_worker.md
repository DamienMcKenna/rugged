---
title: "Feature: A worker to sign TUF repo snapshot metadata."
hidden: True
---

#### Test results for [`features/workers/snapshot_worker.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/workers/snapshot_worker.feature)
Running `behat features/workers/snapshot_worker.feature` results in:

```gherkin
@rugged @workers @snapshot-worker
Feature: A worker to sign TUF repo snapshot metadata.
  In order to securely sign TUF repo snapshot metadata
  As an administrator
  I need to dispatch tasks to a 'snapshot' worker.

  Background:
    Given I reset Rugged

  Scenario: Send a basic ping/echo message.
    When I run "sudo -u rugged rugged echo --worker=snapshot-worker --timeout=1"
    Then I should get:
      """
      Sending snapshot-worker Ping!...
      Done. Response was: snapshot-worker PONG: Ping!
      """

  Scenario: Retrieve worker logs.
    Given I run "sudo -u rugged rugged echo --worker=snapshot-worker --timeout=1"
    When I run "sudo -u rugged rugged logs --worker=snapshot-worker"
    Then I should get:
      """
      INFO (base_worker.echo): snapshot-worker received echo task: Ping!
      """

2 scenarios (2 passed)
7 steps (7 passed)
```
