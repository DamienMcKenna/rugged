---
title: "Feature: Script allows specifying credentials to connect to the queue worker."
hidden: True
---

#### Test results for [`features/commands/echo.creds.cli.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/commands/echo.creds.cli.feature)
Running `behat features/commands/echo.creds.cli.feature` results in:

```gherkin
@rugged @command @echo @credentials @test-worker
Feature: Script allows specifying credentials to connect to the queue worker.
  In order to securely communicate with workers
  As a Rugged administrator
  I need to run scripts using specific credentials.

  Background:
    Given I reset Rugged

  Scenario: Send a ping/echo message, specifying a valid username.
    When I run "sudo -u rugged rugged echo --worker=test-worker --username=rugged-rabbitmq"
    Then I should get:
      """
      Sending test-worker Ping!...
      Done. Response was: test-worker PONG: Ping!
      """
    Then I should not get:
      """
      debug: Initializing connection to RabbitMQ.
      """
    When I run "sudo -u rugged rugged logs --local"
    Then I should get:
      """
      INFO (echo.echo_cmd): Sending test-worker Ping!...
      INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
      """
    Then I should not get:
      """
      DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
      """
    When I run "sudo -u rugged rugged logs --worker=test-worker"
    Then I should get:
      """
      INFO (base_worker.echo): test-worker received echo task: Ping!
      """

  Scenario: Send a ping/echo message, specifying an invalid username.
    When I fail to run "sudo -u rugged rugged --debug echo --worker=test-worker --username=intentionally-invalid"
    Then I should get:
      """
      Sending test-worker Ping!...
      debug: Initializing connection to RabbitMQ.
      debug: RuggedAccessRefused: Failed to authenticate to RabbitMQ.
      error: Failed to authenticate to worker queue. Check credentials.
      """
    When I run "sudo -u rugged rugged logs --local"
    Then I should get:
      """
      INFO (echo.echo_cmd): Sending test-worker Ping!...
      DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
      DEBUG (access_refused.__init__): RuggedAccessRefused: Failed to authenticate to RabbitMQ.
      ERROR (task_queue.run_task): Failed to authenticate to worker queue. Check credentials.
      """
    When I run "sudo -u rugged rugged logs --worker=test-worker"
    Then I should not get:
      """
      INFO (base_worker.echo): test-worker received echo task: Ping!
      """

  Scenario: Send a ping/echo message, specifying a valid password.
    When I run "sudo -u rugged rugged echo --worker=test-worker --password=badpassword"
    Then I should get:
      """
      Sending test-worker Ping!...
      Done. Response was: test-worker PONG: Ping!
      """
    When I run "sudo -u rugged rugged logs --local"
    Then I should get:
      """
      INFO (echo.echo_cmd): Sending test-worker Ping!...
      INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
      """
    When I run "sudo -u rugged rugged logs --worker=test-worker"
    Then I should get:
      """
      INFO (base_worker.echo): test-worker received echo task: Ping!
      """

  Scenario: Send a ping/echo message, specifying an invalid password.
    When I fail to run "sudo -u rugged rugged --debug echo --worker=test-worker --password=intentionally-invalid"
    Then I should get:
      """
      Sending test-worker Ping!...
      debug: Initializing connection to RabbitMQ.
      debug: RuggedAccessRefused: Failed to authenticate to RabbitMQ.
      error: Failed to authenticate to worker queue. Check credentials.
      """
    When I run "sudo -u rugged rugged logs --local"
    Then I should get:
      """
      INFO (echo.echo_cmd): Sending test-worker Ping!...
      DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
      DEBUG (access_refused.__init__): RuggedAccessRefused: Failed to authenticate to RabbitMQ.
      ERROR (task_queue.run_task): Failed to authenticate to worker queue. Check credentials.
      """
    When I run "sudo -u rugged rugged logs --worker=test-worker"
    Then I should not get:
      """
      INFO (base_worker.echo): test-worker received echo task: Ping!
      """

  Scenario: Send a ping/echo message, specifying a valid host.
    When I run "sudo -u rugged rugged echo --worker=test-worker --host=rabbitmq"
    Then I should get:
      """
      Sending test-worker Ping!...
      Done. Response was: test-worker PONG: Ping!
      """
    When I run "sudo -u rugged rugged logs --local"
    Then I should get:
      """
      INFO (echo.echo_cmd): Sending test-worker Ping!...
      INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
      """
    When I run "sudo -u rugged rugged logs --worker=test-worker"
    Then I should get:
      """
      INFO (base_worker.echo): test-worker received echo task: Ping!
      """

  Scenario: Send a ping/echo message, specifying an invalid host.
    When I fail to run "sudo -u rugged rugged --debug echo --worker=test-worker --host=intentionally-invalid"
    Then I should get:
      """
      Sending test-worker Ping!...
      debug: Initializing connection to RabbitMQ.
      debug: RuggedHostnameNotFound: Failed to resolve RabbitMQ hostname.
      error: Failed to resolve worker queue hostname. Check configuration.
      """
    When I run "sudo -u rugged rugged logs --local"
    Then I should get:
      """
      INFO (echo.echo_cmd): Sending test-worker Ping!...
      DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
      DEBUG (hostname_not_found.__init__): RuggedHostnameNotFound: Failed to resolve RabbitMQ hostname.
      ERROR (task_queue.run_task): Failed to resolve worker queue hostname. Check configuration.
      """
    When I run "sudo -u rugged rugged logs --worker=test-worker"
    Then I should not get:
      """
      INFO (base_worker.echo): test-worker received echo task: Ping!
      """

6 scenarios (6 passed)
44 steps (44 passed)
```
