---
title: "Feature: Flag to print debug messages."
hidden: True
---

#### Test results for [`features/commands/debug.feature`](https://gitlab.com/rugged/rugged/-/blob/main/features/commands/debug.feature)
Running `behat features/commands/debug.feature` results in:

```gherkin
@rugged @commands @debug-option @test-worker
Feature: Flag to print debug messages.
  In order to effectively develop Rugged
  As an developer
  I need to get Rugged to emit debug data.

  Background:
    Given I reset Rugged

  Scenario: Debug flag.
    When I run "sudo -u rugged rugged echo"
    Then I should not get:
      """
      debug: rugged_cli invoked
      """
    When I run "sudo -u rugged rugged --debug echo"
    Then I should get:
      """
      debug: rugged_cli invoked
      """

  Scenario: Debug flag propagates to logs.
    When I run "sudo -u rugged rugged echo"
    And I run "sudo -u rugged rugged logs --local"
    Then I should not get:
      """
      DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
      """
    When I run "sudo -u rugged rugged --debug echo"
    And I run "sudo -u rugged rugged logs --local"
    Then I should get:
      """
      DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
      """

  Scenario: Debug flag propagates to sub-commands.
    When I run "sudo -u rugged rugged logs --worker test-worker"
    Then I should not get:
      """
      debug: Initializing connection to RabbitMQ.
      """
    When I run "sudo -u rugged rugged --debug logs --worker test-worker"
    Then I should get:
      """
      debug: Initializing connection to RabbitMQ.
      """

  Scenario: Debug flag propagates to workers' logs.
    When I run "sudo -u rugged rugged echo --worker test-worker"
    When I run "sudo -u rugged rugged logs --worker test-worker"
    Then I should not get:
      """
      DEBUG (base_worker.echo): test-worker received debug flag.
      """
    When I run "sudo -u rugged rugged --debug echo --worker test-worker"
    When I run "sudo -u rugged rugged logs --worker test-worker"
    Then I should get:
      """
      DEBUG (base_worker.echo): test-worker received debug flag.
      """

4 scenarios (4 passed)
24 steps (24 passed)
```
