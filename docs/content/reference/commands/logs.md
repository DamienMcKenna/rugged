---
title: "rugged logs"
hidden: True
---

#### Command help text
```terminal
$ rugged logs --help

Usage: rugged logs [OPTIONS]

  Print the logs for a TUF repository.

Options:
  --local          Print the local logs. Defaults to true, unless a worker is
                   specified.
  --worker TEXT    The specific worker from which to retrieve logs. Can be
                   passed multiple times.
  --limit INTEGER  The number of lines to print for each log.
  --truncate       Instead of printing logs, empty log file(s).
  --help           Show this message and exit.
```

#### Command example output
```terminal
$ rugged logs --worker=test-worker

=== Log for test-worker: /var/log/rugged/rugged.log ===
2022-04-15 21:57:43,187 INFO (base_worker.echo): test-worker received echo task: Ping!
```
