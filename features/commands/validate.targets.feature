@rugged @command @validate @validate-targets
Feature: Validate TUF targets metadata.
  In order to check the integrity of the files in a TUF repository,
  As a TUF Administrator,
  I need to validate TUF targets metadata.

  Background:
    Given I rebuild fixtures

  @wip
  Scenario: Validate a specific target's metadata.
     When I run "sudo -u rugged rugged validate --target=foo/bar.txt"

  @wip
  Scenario: Validate all target metadata.
     When I run "sudo -u rugged rugged validate --all-targets"
