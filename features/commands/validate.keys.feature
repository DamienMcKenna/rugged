@rugged @command @validate @validate-keys
Feature: Validate TUF repository metadata.
  In order to ensure the integrity of the TUF repository,
  As a TUF Administrator,
  I need to validate specific aspects of TUF metadata.

  Background:
    Given I reset Rugged

  Scenario: Report keys found in TUF repository.
    Given I run "sudo cp features/fixtures/key_rotation/no_change.yaml /var/rugged/.config/rugged/config.yaml"
      And I run "sudo -u rugged rugged generate-keys --local"
      And I run "sudo -u rugged rugged initialize --local"
     When I run "sudo -u rugged rugged --debug validate"
     Then I should get:
          """
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: Only configured keys for 'snapshot' role exist in storage.
          debug: Only configured keys for 'targets' role exist in storage.
          Only expected keys are present.
          debug: Root metadata contains the 'root' key for the 'root' role.
          debug: Root metadata contains the 'root1' key for the 'root' role.
          debug: Root metadata contains the 'timestamp' key for the 'timestamp' role.
          debug: Root metadata contains the 'snapshot' key for the 'snapshot' role.
          debug: Root metadata contains the 'targets' key for the 'targets' role.
          debug: Root metadata contains all configured keys.
          debug: Root metadata contains only configured keys.
          All keys in repository are valid.
          """
      And I should not get:
          """
          error: An expected key was not found. Check logs for details.
          error: An unexpected key was found. Check logs for details.
          """

  Scenario: Flag a stale or missing verification key.
    Given I run "sudo cp features/fixtures/key_rotation/flag_removed_key.yaml /var/rugged/.config/rugged/config.yaml"
      And I run "sudo -u rugged rugged generate-keys --local"
      And I run "sudo -u rugged rugged initialize --local"
      And I run "sudo rm fixtures/verification_keys/snapshot/snapshot1.pub"
     When I try to run "sudo -u rugged rugged --debug validate"
     Then I should not get:
          """
          debug: All configured keys for 'snapshot' role exist in storage.
          All expected keys are present.
          """
     Then I should get:
          """
          debug: All configured keys for 'root' role exist in storage.
          debug: RuggedKeyError: The 'snapshot1' verification key configured for the 'snapshot' role was not found in storage.
          error: RuggedKeyError thrown in _validate_keys: The 'snapshot1' verification key configured for the 'snapshot' role was not found in storage.
          error: An expected key was not found. Check logs for details.
          """

  Scenario: Flag a stale or missing signing key.
    Given I run "sudo cp features/fixtures/key_rotation/flag_removed_key.yaml /var/rugged/.config/rugged/config.yaml"
      And I run "sudo -u rugged rugged generate-keys --local"
      And I run "sudo -u rugged rugged initialize --local"
      And I run "sudo rm fixtures/signing_keys/snapshot/snapshot1"
     When I try to run "sudo -u rugged rugged --debug validate"
     Then I should not get:
          """
          debug: All configured keys for 'snapshot' role exist in storage.
          All expected keys are present.
          """
     Then I should get:
          """
          debug: All configured keys for 'root' role exist in storage.
          debug: RuggedKeyError: The 'snapshot1' signing key configured for the 'snapshot' role was not found in storage.
          error: RuggedKeyError thrown in _validate_keys: The 'snapshot1' signing key configured for the 'snapshot' role was not found in storage.
          error: An expected key was not found. Check logs for details.
          """

  Scenario: Flag an unexpected verification key.
    Given I initialize a Rugged repo
      And I run "sudo cp features/fixtures/keys/dummy_key.pub fixtures/verification_keys/snapshot/snapshot2.pub"
     When I try to run "sudo -u rugged rugged --debug validate"
     Then I should get:
          """
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: RuggedKeyError: The 'snapshot2' key for the 'snapshot' role found in storage does not exist in configuration.
          error: RuggedKeyError thrown in _validate_keys: The 'snapshot2' key for the 'snapshot' role found in storage does not exist in configuration.
          error: An unexpected key was found. Check logs for details.
          """

  Scenario: Flag an unexpected signing key.
    Given I initialize a Rugged repo
      And I run "sudo cp features/fixtures/keys/dummy_key fixtures/signing_keys/snapshot/snapshot2"
     When I try to run "sudo -u rugged rugged --debug validate"
     Then I should get:
          """
          debug: All configured keys for 'root' role exist in storage.
          debug: All configured keys for 'snapshot' role exist in storage.
          debug: All configured keys for 'targets' role exist in storage.
          debug: All configured keys for 'timestamp' role exist in storage.
          All expected keys are present.
          debug: Found 'root' verification key for 'root' role.
          debug: Found 'root1' verification key for 'root' role.
          debug: Found 'timestamp' verification key for 'timestamp' role.
          debug: Found 'snapshot' verification key for 'snapshot' role.
          debug: Found 'snapshot2' signing key for 'snapshot' role.
          debug: Found 'targets' verification key for 'targets' role.
          debug: Only configured keys for 'root' role exist in storage.
          debug: Only configured keys for 'timestamp' role exist in storage.
          debug: RuggedKeyError: The 'snapshot2' key for the 'snapshot' role found in storage does not exist in configuration.
          error: RuggedKeyError thrown in _validate_keys: The 'snapshot2' key for the 'snapshot' role found in storage does not exist in configuration.
          error: An unexpected key was found. Check logs for details.
          """
