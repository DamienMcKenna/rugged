@rugged @command @echo @root-worker @snapshot-worker @targets-worker @test-worker @timestamp-worker
Feature: Script to ping a queue worker.
  In order to test that a worker is running
  As a developer
  I need to receive a ping/echo response.

  Background:
    Given I reset Rugged

  Scenario: Send a ping/echo message, specifying a valid worker.
     When I run "sudo -u rugged rugged echo --worker=test-worker"
     Then I should get:
          """
          Sending test-worker Ping!...
          Done. Response was: test-worker PONG: Ping!
          """
     Then I should not get:
          """
          debug: Initializing connection to RabbitMQ.
          """
     When I run "sudo -u rugged rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
          """
     Then I should not get:
          """
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          """
     When I run "sudo -u rugged rugged logs --worker=test-worker"
     Then I should get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """

  Scenario: Send a ping/echo message, specifying an invalid worker.
     When I fail to run "sudo -u rugged rugged --debug echo --worker=intentionally-invalid"
     Then I should get:
          """
          Sending intentionally-invalid Ping!...
          error: The operation timed out. Check status of intentionally-invalid.
          """
     When I run "sudo -u rugged rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending intentionally-invalid Ping!...
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          DEBUG (timeout_error.__init__): RuggedTimeoutError: The operation exceeded the given deadline.
          ERROR (task_queue.run_task): The operation timed out. Check status of intentionally-invalid.
          """
     When I run "sudo -u rugged rugged logs --worker=test-worker"
     Then I should not get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """

  Scenario: Send a ping/echo message, specifying the message.
     When I run "sudo -u rugged rugged echo --worker=test-worker --message=Not-ping"
     Then I should get:
          """
          Sending test-worker Not-ping...
          Done. Response was: test-worker PONG: Not-ping
          """
     Then I should not get:
          """
          debug: Initializing connection to RabbitMQ.
          """
     When I run "sudo -u rugged rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Not-ping...
          INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Not-ping
          """
     Then I should not get:
          """
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          """
     When I run "sudo -u rugged rugged logs --worker=test-worker"
     Then I should get:
          """
          INFO (base_worker.echo): test-worker received echo task: Not-ping
          """

  Scenario: Send a ping/echo message, specifying multiple workers.
     When I run "sudo -u rugged rugged echo --worker=test-worker --worker=root-worker"
     Then I should get:
          """
          Sending test-worker Ping!...
          Done. Response was: test-worker PONG: Ping!
          """
     Then I should not get:
          """
          debug: Initializing connection to RabbitMQ.
          """
     When I run "sudo -u rugged rugged logs --local"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
          """
     Then I should not get:
          """
          DEBUG (task_queue.__init__): Initializing connection to RabbitMQ.
          """
     When I run "sudo -u rugged rugged logs --worker=test-worker"
     Then I should get:
          """
          INFO (base_worker.echo): test-worker received echo task: Ping!
          """
  Scenario: Send a basic ping/echo message to all workers.
     When I run "sudo -u rugged rugged echo"
     Then I should get:
          """
          Sending root-worker Ping!...
          Done. Response was: root-worker PONG: Ping!
          Sending snapshot-worker Ping!...
          Done. Response was: snapshot-worker PONG: Ping!
          Sending targets-worker Ping!...
          Done. Response was: targets-worker PONG: Ping!
          Sending test-worker Ping!...
          Done. Response was: test-worker PONG: Ping!
          Sending timestamp-worker Ping!...
          Done. Response was: timestamp-worker PONG: Ping!
          """
     When I run "sudo -u rugged rugged logs --local --limit=25"
     Then I should get:
          """
          INFO (echo.echo_cmd): Sending root-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: root-worker PONG: Ping!
          INFO (echo.echo_cmd): Sending snapshot-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: snapshot-worker PONG: Ping!
          INFO (echo.echo_cmd): Sending targets-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: targets-worker PONG: Ping!
          INFO (echo.echo_cmd): Sending test-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: test-worker PONG: Ping!
          INFO (echo.echo_cmd): Sending timestamp-worker Ping!...
          INFO (echo.echo_cmd): Done. Response was: timestamp-worker PONG: Ping!
          """
     When I run "sudo -u rugged rugged logs --limit=25"
     Then I should get:
          """
          INFO (base_worker.echo): root-worker received echo task: Ping!
          INFO (base_worker.echo): snapshot-worker received echo task: Ping!
          INFO (base_worker.echo): targets-worker received echo task: Ping!
          INFO (base_worker.echo): test-worker received echo task: Ping!
          INFO (base_worker.echo): timestamp-worker received echo task: Ping!
          """
