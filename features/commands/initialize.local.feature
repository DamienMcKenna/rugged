@rugged @commands @initialize @local
Feature: Command to initialize Rugged TUF repository locally.
  In order to host TUF metadata
  As an administrator
  I need to initialize a TUF repo locally.

  Background:
    Given I reset Rugged

  Scenario: Initialize repository locally.
     When I run "sudo -u rugged rugged generate-keys --local"
     When I try to run "sudo -u rugged rugged --debug initialize --local"
     Then I should get:
          """
          Initializing new TUF repository at /var/rugged/tuf_repo.
          TUF repository initialized.
          """
      And the "/var/rugged/tuf_repo/targets" directory should exist
     When I run "sudo -u rugged rugged logs --local --limit=0"
     Then I should get:
          """
          DEBUG (init_repo.init_repo): Initializing new TUF repository locally.
          INFO (init_repo.init_repo): Initializing new TUF repository at /var/rugged/tuf_repo.
          DEBUG (repo.__init__): Instantiated repository.
          DEBUG (repo._sign_metadata): Signed 'targets' metadata with 'targets' key.
          DEBUG (repo.write_metadata): Wrote 'targets' metadata to file '/var/rugged/tuf_repo/metadata/targets.json'.
          DEBUG (repo._sign_metadata): Signed 'snapshot' metadata with 'snapshot' key.
          DEBUG (repo.write_metadata): Wrote 'snapshot' metadata to file '/var/rugged/tuf_repo/metadata/snapshot.json'.
          DEBUG (repo._sign_metadata): Signed 'timestamp' metadata with 'timestamp' key.
          DEBUG (repo.write_metadata): Wrote 'timestamp' metadata to file '/var/rugged/tuf_repo/metadata/timestamp.json'.
          DEBUG (repo._sign_metadata): Signed 'root' metadata with 'root' key.
          DEBUG (repo._sign_metadata): Signed 'root' metadata with 'root1' key.
          DEBUG (repo.write_metadata): Wrote 'root' metadata to file '/var/rugged/tuf_repo/metadata/1.root.json'.
          INFO (initialize.initialize_cmd): TUF repository initialized.
          """
     When I run "sudo -u rugged rugged logs --worker=root-worker --limit=0"
     Then I should not get:
          """
          DEBUG (root-worker.initialize): Received 'initialize' task.
          INFO (root-worker.initialize): Initializing new TUF repository at /var/rugged/tuf_repo.
          DEBUG (init_repo.init_tuf_repo): Path to initialize repo loaded from config: /var/rugged/tuf_repo
          INFO (root-worker.initialize): TUF repository initialized.
          """
     When I run "ls /var/rugged/tuf_repo"
     Then I should get:
          """
          metadata
          targets
          """
      And I should not get:
          """
          Permission denied
          """
     When I run "ls /var/rugged/tuf_repo/metadata"
     Then I should get:
          """
          1.root.json
          """

  Scenario: Fail gracefully on initialization errors.
    Given I run "sudo chmod 000 /var/rugged/tuf_repo"
     When I fail to run "sudo -u rugged rugged --debug initialize --local"
     Then I should get:
          """
          error: Failed to initialize repository at /var/rugged/tuf_repo.
          Failed to initialize TUF repository. Check the logs for more detailed error reporting.
          """
      And I should not get:
          """
          TUF repository initialized.
          """
     When I run "sudo -u rugged rugged logs --local"
     Then I should get:
          """
          DEBUG (init_repo.init_repo): Initializing new TUF repository locally.
          INFO (init_repo.init_repo): Initializing new TUF repository at /var/rugged/tuf_repo.
          DEBUG (storage_error.__init__): RuggedStorageError: The repository could not create the needed directories.
          ERROR (logger.log_exception): RuggedStorageError thrown in __init__: The repository could not create the needed directories.
          ERROR (repo.__init__): Failed to instantiate repository.
          DEBUG (repository_error.__init__): RuggedRepositoryError: The repository could not load a repository at the given path.
          ERROR (logger.log_exception): RuggedRepositoryError thrown in init_repo: The repository could not load a repository at the given path.
          ERROR (initialize.initialize_cmd): Failed to initialize repository at /var/rugged/tuf_repo.
          """
      And I should not get:
          """
          INFO (initialize.initialize_cmd): TUF repository initialized.
          """

  Scenario: Fail gracefully on metadata errors.
    Given I run "sudo -u rugged rugged generate-keys --role=root"
      And I run "sudo -u rugged rugged generate-keys --role=snapshot"
      And I run "sudo -u rugged rugged generate-keys --role=timestamp"
     When I fail to run "sudo -u rugged rugged --debug initialize --local"
     Then I should get:
          """
          debug: Initializing new TUF repository locally.
          Initializing new TUF repository at /var/rugged/tuf_repo.
          error: ValueError thrown in __init__: Role names must be the top-level metadata roles
          error: Failed to instantiate repository.
          debug: RuggedRepositoryError: The repository could not load a repository at the given path.
          error: RuggedRepositoryError thrown in init_repo: The repository could not load a repository at the given path.
          error: Failed to initialize repository at /var/rugged/tuf_repo.
          Failed to initialize TUF repository. Check the logs for more detailed error reporting.
          """
      And I should not get:
          """
          TUF repository initialized.
          """
     When I run "sudo -u rugged rugged logs --local --limit=0"
     Then I should get:
          """
          DEBUG (init_repo.init_repo): Initializing new TUF repository locally.
          INFO (init_repo.init_repo): Initializing new TUF repository at /var/rugged/tuf_repo.
          ERROR (logger.log_exception): ValueError thrown in __init__: Role names must be the top-level metadata roles
          ERROR (repo.__init__): Failed to instantiate repository.
          DEBUG (repository_error.__init__): RuggedRepositoryError: The repository could not load a repository at the given path.
          ERROR (logger.log_exception): RuggedRepositoryError thrown in init_repo: The repository could not load a repository at the given path.
          ERROR (initialize.initialize_cmd): Failed to initialize repository at /var/rugged/tuf_repo.
          """
      And I should not get:
          """
          INFO (initialize.initialize_cmd): TUF repository initialized.
          """
