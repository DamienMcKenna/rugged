@rugged @command @add-targets
Feature: Command to add targets to the TUF repository.
  In order to host TUF metadata
  As an administrator
  I need to add targets to a TUF repo.

  Background:
    Given I reset Rugged
      And I initialize a Rugged repo
      And I am in the "/var/rugged/incoming_targets" directory

  Scenario: Add a target file.
    Given file "test0.txt" contains "test0"
     When I run "sudo -u rugged rugged --debug add-targets"
     Then I should get:
          """
          Added the following targets to the repository:
          test0.txt
          Updated targets metadata.
          Updated snapshot metadata.
          Updated timestamp metadata.
          """
     When I run "grep -r test0.txt ../tuf_repo/metadata"
     Then I should get:
          """
          targets.json
          """
     When I run "ls"
     Then I should not get:
          """
          Permission denied
          test0.txt
          """
     When I run "sudo -u rugged rugged logs --worker=targets-worker --limit=0"
     Then I should get:
          """
          INFO (targets-worker.add_targets_task): Received add-targets task.
          DEBUG (repo.get_inbound_targets): Scanning for inbound targets in '/var/rugged/incoming_targets'
          DEBUG (repo.get_inbound_targets): Found target: test0.txt
          DEBUG (repo._move_inbound_target_to_targets_dir): Moved '/var/rugged/incoming_targets/test0.txt' to '/var/rugged/tuf_repo/targets/test0.txt'
          INFO (repo._move_inbound_target_to_targets_dir): Moved inbound target 'test0.txt' to targets directory.
          INFO (repo.add_targets): Added target 'test0.txt' to the repository.
          DEBUG (repo._sign_metadata): Signed 'targets' metadata with 'targets' key.
          DEBUG (repo.write_metadata): Wrote 'targets' metadata to file '/var/rugged/tuf_repo/metadata/targets.json'.
          """
     When I run "sudo -u rugged rugged logs --worker=snapshot-worker --limit=0"
     Then I should get:
          """
          INFO (snapshot-worker.update_snapshot_task): Received update-snapshot task.
          DEBUG (repo._sign_metadata): Signed 'snapshot' metadata with 'snapshot' key.
          DEBUG (repo.write_metadata): Wrote 'snapshot' metadata to file '/var/rugged/tuf_repo/metadata/snapshot.json'.
          INFO (snapshot-worker.update_snapshot_task): Updated snapshot metadata.
          """
     When I run "sudo -u rugged rugged logs --worker=timestamp-worker --limit=0"
     Then I should get:
          """
          INFO (timestamp-worker.update_timestamp_task): Received update-timestamp task.
          DEBUG (repo._sign_metadata): Signed 'timestamp' metadata with 'timestamp' key.
          DEBUG (repo.write_metadata): Wrote 'timestamp' metadata to file '/var/rugged/tuf_repo/metadata/timestamp.json'.
          INFO (timestamp-worker.update_timestamp_task): Updated timestamp metadata.
          """

  Scenario: Verify that metadata is updated.
      And file "test0.txt" contains "test0"
      And I record a reference hash of "../tuf_repo/metadata/1.root.json"
      And the file "../tuf_repo/metadata/2.root.json" does not exist
      And I record a reference hash of "../tuf_repo/metadata/snapshot.json"
      And I record a reference hash of "../tuf_repo/metadata/targets.json"
      And I record a reference hash of "../tuf_repo/metadata/timestamp.json"
     When I run "sudo -u rugged rugged add-targets"
     Then file "../tuf_repo/metadata/1.root.json" has not changed
      And the file "../tuf_repo/metadata/2.root.json" does not exist
      And file "../tuf_repo/metadata/targets.json" has changed
      And file "../tuf_repo/metadata/snapshot.json" has changed
      And file "../tuf_repo/metadata/timestamp.json" has changed

  Scenario: Add multiple target files, including one in a subdirectory.
    Given file "test0.txt" contains "test0"
      And file "test1.txt" contains "test1"
      And the directory "foo/bar" exists
      And file "foo/bar/test2.txt" contains "test2"
     When I run "sudo -u rugged rugged --debug add-targets"
     Then I should get:
          """
          Added the following targets to the repository:
          test0.txt
          test1.txt
          foo/bar/test2.txt
          Updated targets metadata.
          Updated snapshot metadata.
          Updated timestamp metadata.
          """
     When I run "grep -r test0.txt ../tuf_repo/metadata"
     Then I should get:
          """
          targets.json
          """
     When I run "grep -r test1.txt ../tuf_repo/metadata"
     Then I should get:
          """
          targets.json
          """
     When I run "grep -r foo/bar/test2.txt ../tuf_repo/metadata"
     Then I should get:
          """
          targets.json
          """
     When I run "ls"
     Then I should not get:
          """
          test0.txt
          test1.txt
          foo
          """
    Given I am in the "/var/rugged/tuf_repo/targets" directory
     When I run "ls"
     Then I should get:
          """
          test0.txt
          test1.txt
          foo
          """
     When I run "sudo -u rugged rugged logs --worker=targets-worker --limit=0"
     Then I should get:
          """
          INFO (targets-worker.add_targets_task): Received add-targets task.
          DEBUG (repo.get_inbound_targets): Scanning for inbound targets in '/var/rugged/incoming_targets'
          DEBUG (repo.get_inbound_targets): Found target: test1.txt
          DEBUG (repo.get_inbound_targets): Found target: test0.txt
          DEBUG (repo.get_inbound_targets): Found target: foo/bar/test2.txt
          DEBUG (repo._move_inbound_target_to_targets_dir): Moved '/var/rugged/incoming_targets/test1.txt' to '/var/rugged/tuf_repo/targets/test1.txt'
          INFO (repo._move_inbound_target_to_targets_dir): Moved inbound target 'test1.txt' to targets directory.
          INFO (repo.add_targets): Added target 'test1.txt' to the repository.
          DEBUG (repo._move_inbound_target_to_targets_dir): Moved '/var/rugged/incoming_targets/test0.txt' to '/var/rugged/tuf_repo/targets/test0.txt'
          INFO (repo._move_inbound_target_to_targets_dir): Moved inbound target 'test0.txt' to targets directory.
          INFO (repo.add_targets): Added target 'test0.txt' to the repository.
          DEBUG (repo._move_inbound_target_to_targets_dir): Moved '/var/rugged/incoming_targets/foo/bar/test2.txt' to '/var/rugged/tuf_repo/targets/foo/bar/test2.txt'
          DEBUG (repo._delete_empty_target_dirs): Cleaned up empty directory '/var/rugged/incoming_targets/foo/bar'.
          DEBUG (repo._delete_empty_target_dirs): Cleaned up empty directory '/var/rugged/incoming_targets/foo'.
          INFO (repo._move_inbound_target_to_targets_dir): Moved inbound target 'foo/bar/test2.txt' to targets directory.
          INFO (repo.add_targets): Added target 'foo/bar/test2.txt' to the repository.
          DEBUG (repo._sign_metadata): Signed 'targets' metadata with 'targets' key.
          DEBUG (repo.write_metadata): Wrote 'targets' metadata to file '/var/rugged/tuf_repo/metadata/targets.json'.
          """

  Scenario: Add multiple target files, in separate invocations.
    Given file "test0.txt" contains "test0"
     When I run "sudo -u rugged rugged --debug add-targets"
     Then I should get:
          """
          Added the following targets to the repository:
          test0.txt
          Updated targets metadata.
          Updated snapshot metadata.
          Updated timestamp metadata.
          """
     When I run "grep -r test0.txt ../tuf_repo/metadata"
     Then I should get:
          """
          targets.json
          """
     When I try to run "grep -r test1.txt ../tuf_repo/metadata"
     Then I should not get:
          """
          targets.json
          """
    Given I am in the "/var/rugged/tuf_repo/targets" directory
     When I run "ls"
     Then I should get:
          """
          test0.txt
          """
      And I should not get:
          """
          test1.txt
          """
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "test1.txt" contains "test1"
     When I run "sudo -u rugged rugged --debug add-targets"
     Then I should get:
          """
          Added the following targets to the repository:
          test1.txt
          Updated targets metadata.
          Updated snapshot metadata.
          Updated timestamp metadata.
          """
     When I run "grep -r test0.txt ../tuf_repo/metadata"
     Then I should get:
          """
          targets.json
          """
     When I run "grep -r test1.txt ../tuf_repo/metadata"
     Then I should get:
          """
          targets.json
          """
    Given I am in the "/var/rugged/tuf_repo/targets" directory
     When I run "ls"
     Then I should get:
          """
          test0.txt
          test1.txt
          """
