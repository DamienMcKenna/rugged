@rugged @command @validate @validate-metadata
Feature: Validate TUF repository metadata.
  In order to check the integrity of the TUF repository,
  As a TUF Administrator,
  I need to validate TUF metadata.

  Background:
    Given I rebuild fixtures

  Scenario: Validate top-level metadata.
     When I run "sudo -u rugged rugged validate"
     Then I should get:
     """
     Metadata for the 'root' role is valid.
     Metadata for the 'timestamp' role is valid.
     Metadata for the 'snapshot' role is valid.
     Metadata for the 'targets' role is valid.
     """
     And I should not get:
     """
     error: Metadata for the 'root' role is not valid.
     error: Metadata for the 'timestamp' role is not valid.
     error: Metadata for the 'snapshot' role is not valid.
     error: Metadata for the 'targets' role is not valid.
     """

  Scenario: Flag invalid top-level metadata.
    Given I run "sudo cp features/fixtures/metadata/invalid_targets.json fixtures/tuf_repo/metadata/targets.json"
     When I try to run "sudo -u rugged rugged validate"
     Then I should get:
     """
     Metadata for the 'root' role is valid.
     Metadata for the 'timestamp' role is valid.
     Metadata for the 'snapshot' role is valid.
     error: Metadata for the 'targets' role is not valid.
     """
     And I should not get:
     """
     Metadata for the 'targets' role is valid.
     """
    Given I run "sudo cp features/fixtures/metadata/invalid_snapshot.json fixtures/tuf_repo/metadata/snapshot.json"
     When I try to run "sudo -u rugged rugged validate"
     Then I should get:
     """
     Metadata for the 'root' role is valid.
     Metadata for the 'timestamp' role is valid.
     error: Metadata for the 'snapshot' role is not valid.
     """
     And I should not get:
     """
     Metadata for the 'snapshot' role is valid.
     """
    Given I run "sudo cp features/fixtures/metadata/invalid_timestamp.json fixtures/tuf_repo/metadata/timestamp.json"
     When I try to run "sudo -u rugged rugged validate"
     Then I should get:
     """
     Metadata for the 'root' role is valid.
     error: Metadata for the 'timestamp' role is not valid.
     """
     And I should not get:
     """
     Metadata for the 'timestamp' role is valid.
     """
    Given I run "sudo cp features/fixtures/metadata/invalid_root.json fixtures/tuf_repo/metadata/1.root.json"
     When I try to run "sudo -u rugged rugged validate"
     Then I should get:
     """
     error: Metadata for the 'root' role is not valid.
     """
     And I should not get:
     """
     Metadata for the 'root' role is valid.
     """

  @wip
  Scenario: Validate metadata after key rotation.

  @wip
  Scenario: Flag invalid metadata after key rotation.
