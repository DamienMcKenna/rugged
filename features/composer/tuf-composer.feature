@composer-plugin @php-tuf @satis
Feature: The TUF Composer plugin works with Satis repositories.
  In order to securely build PHP codebases
  As a site builder
  I need Composer to install and use the TUF Composer plugin.

  Background:
    Given I run "make reset-satis"
      And I am in the "satis" directory
      And I run "bin/satis build"
      And I am in the "../d9-site" directory
      And I run "ln -sf tuf-satis-composer.json composer.json"

  Scenario: Composer TUF plugin is installed.
    Given I run "ln -sf tuf-composer.json composer.json"
      And I run "rm -rf vendor/php-tuf"
      And the file "composer.json" contains:
          """
          "url": "https://github.com/php-tuf/composer-integration"
          "url": "https://github.com/php-tuf/php-tuf"
          "php-tuf/composer-integration": "*"
          "github-oauth": {
          "minimum-stability": "dev",
          """
     When I run "composer install"
     Then I should not get:
          """
          Exit Code: 1(General error)
          Root composer.json requires php-tuf/composer-integration, it could not be found in any version, there may be a typo in the package name.
          require php-tuf/php-tuf dev-main -> could not be found in any version, there may be a typo in the package name.
          Exit Code: 255(Unknown error)
          Could not authenticate against github.com
          Exit Code: 2(Misuse of shell builtins)
          require php-tuf/php-tuf dev-main -> found php-tuf/php-tuf[dev-main] but it does not match your minimum-stability.
          Authenticity of packages from http://packages.ddev.site are not verified by TUF.
          """
     Then I should get:
          """
          Installing php-tuf/composer-integration
          Authenticity of packages from https://repo.packagist.org are not verified by TUF.
          """

  Scenario: Composer w/ TUF plugin can access a Satis repo.
    Given the file "composer.json" contains:
          """
          "url": "http://packages.ddev.site"
          """
     When I run "composer config --list | grep repositories"
     Then I should get:
          """
          http://packages.ddev.site
          """

  Scenario: Composer w/ TUF plugin can install a package from a Satis repo.
     When I run "composer install"
     Then I should get:
          """
          - Locking drupal/token (1.9.0)
          Installing drupal/token (1.9.0): Extracting archive
          Authenticity of packages from http://packages.ddev.site are not verified by TUF.
          """
      And the following files should exist:
          """
          web/modules/contrib/token/token.info.yml
          """
