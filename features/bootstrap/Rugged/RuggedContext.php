<?php
namespace Rugged;

use Consensus\TerminalContext;

/**
 * Defines application features from the specific context.
 */
class RuggedContext extends TerminalContext {

  /**
   * @Given I rebuild fixtures
   */
  public function iRebuildFixtures()
  {
    $this->runSteps([
      'make clean-fixtures',
      'make fixtures',
    ]);
  }

  /**
   * @Given I reset Rugged
   */
  public function iResetRugged()
  {
    $this->runSteps([
      'make reset-rugged',
    ]);
  }

  /**
   * @Given I initialize a Rugged repo
   */
  public function iInitializeRuggedRepo()
  {
    $this->runSteps([
      'make init-rugged-repo',
    ]);
  }

  /**
   * @Given I publish TUF Metadata
   */
  public function iPublishTufMetadata()
  {
    $this->runSteps([
      'make publish-tuf-metadata',
    ]);
  }

  /**
   * @Given I register the Rugged repo with Composer
   */
  public function iRegisterRuggedRepoWithComposer()
  {
    $this->runSteps([
      'make register-tuf-repo',
    ]);
  }

  /**
   * @Given I use the :composer_file Composer file
   */
  public function iUseComposerFile($composer_file)
  {
    $this->runSteps([
      "cd d9-site; ln -sf {$composer_file} composer.json",
    ]);
  }

  /**
   * @Given file :file contains :content
   */
  public function fileContains($file, $content)
  {
    $this->runSteps([
      "sudo sudo -u rugged sh -c \"echo '${content}' > ${file}\"",  // Create the file as 'rugged'
    ]);
  }

  /**
   * @Given the directory :directory exists
   */
  public function directoryExists($directory)
  {
    $this->runSteps([
      "sudo sudo -u rugged mkdir -p ${directory}",  // Create the directory as 'rugged'
    ]);
  }

}
