@rugged @security @metadata-expiry
Feature: Configurable metadata expiry per role.
  In order to keep a TUF systems secure,
  As a TUF administrator
  I need to be able to configure each role's expiry separately.

  Background:
    Given I rebuild fixtures

  Scenario: Default metadata expiry is reasonable.
     When I run "sudo -u rugged rugged status --local"
     Then I should get:
          """
          Role       Capability    Signatures      Version  TUF Spec    Expires
          ---------  ------------  ------------  ---------  ----------  ---------------------------------
          targets    Signing       1 / 1                 2  1.0.28      6 days, 23 hours
          snapshot   Signing       1 / 1                 2  1.0.28      6 days, 23 hours
          timestamp  Signing       1 / 1                 2  1.0.28      23 hours
          root       Signing       2 / 1                 1  1.0.28      364 days, 23 hours
          """

  Scenario: Roles can be configured to require multiple signatures.
    Given I reset Rugged
      And I run "sudo -u rugged rugged generate-keys"
      And I run "sudo cp features/fixtures/config/metadata_expiry.yaml /var/rugged/.config/rugged/config.yaml"
      And I run "sudo -u rugged rugged --debug initialize --local"
     When I run "sudo -u rugged rugged logs --local --limit=0"
     Then I should get:
          """
          DEBUG (repo._init_targets_role): Setting 'targets' metadata expiry to 8.
          DEBUG (repo._init_snapshot_role): Setting 'snapshot' metadata expiry to 8.
          DEBUG (repo._init_timestamp_role): Setting 'timestamp' metadata expiry to 2.
          DEBUG (repo._init_root_role): Setting 'root' metadata expiry to 366.
          """
     When I run "sudo -u rugged rugged status --local"
     Then I should get:
          """
          Role       Capability    Signatures      Version  TUF Spec    Expires
          ---------  ------------  ------------  ---------  ----------  -------------------------------
          targets    Signing       1 / 1                 1  1.0.28      7 days, 23 hours
          snapshot   Signing       1 / 1                 1  1.0.28      7 days, 23 hours
          timestamp  Signing       1 / 1                 1  1.0.28      1 day, 23 hours
          root       Signing       2 / 1                 1  1.0.28      1 year, 23 hours
          """
