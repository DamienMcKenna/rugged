@rugged @security @signature-thresholds
Feature: Configurable signature thresholds per role.
  In order to keep a TUF systems secure,
  As a TUF administrator
  I need to be able to configure each role's signature threshold separately.

  Background:
    Given I rebuild fixtures

  Scenario: By default, each role only requires a single signature.
     When I run "sudo -u rugged rugged status --worker=root-worker"
     Then I should get:
          """
          === Repository status for root-worker ===
          Role       Capability    Signatures
          ---------  ------------  ------------
          targets    Signing       1 / 1
          snapshot   Signing       1 / 1
          timestamp  Signing       1 / 1
          root       Signing       2 / 1
          """
      And I should not get:
          """
          === Repository status for timestamp-worker ===
          === Repository status for snapshot-worker ===
          === Repository status for targets-worker ===
          """
     When I run "sudo -u rugged rugged status --worker=timestamp-worker"
     Then I should get:
          """
          === Repository status for timestamp-worker ===
          Role       Capability    Signatures
          ---------  ------------  ------------
          targets    Verification  1 / 1
          snapshot   Verification  1 / 1
          timestamp  Signing       1 / 1
          root       Verification  2 / 1
          """
      And I should not get:
          """
          === Repository status for root-worker ===
          === Repository status for snapshot-worker ===
          === Repository status for targets-worker ===
          """
     When I run "sudo -u rugged rugged status --worker=snapshot-worker"
     Then I should get:
          """
          === Repository status for snapshot-worker ===
          Role       Capability    Signatures
          ---------  ------------  ------------
          targets    Verification  1 / 1
          snapshot   Signing       1 / 1
          timestamp  Verification  1 / 1
          root       Verification  2 / 1
          """
      And I should not get:
          """
          === Repository status for root-worker ===
          === Repository status for timestamp-worker ===
          === Repository status for targets-worker ===
          """
     When I run "sudo -u rugged rugged status --worker=targets-worker"
     Then I should get:
          """
          === Repository status for targets-worker ===
          Role       Capability    Signatures
          ---------  ------------  ------------
          targets    Signing       1 / 1
          snapshot   Verification  1 / 1
          timestamp  Verification  1 / 1
          root       Verification  2 / 1
          """
      And I should not get:
          """
          === Repository status for root-worker ===
          === Repository status for timestamp-worker ===
          === Repository status for snapshot-worker ===
          """
     When I run "cat fixtures/tuf_repo/metadata/1.root.json"
     Then I should get:
          """
          "threshold": 1
          """
      And I should not get:
          """
          "threshold": 2
          """

  Scenario: Roles can be configured to require multiple signatures.
    Given I reset Rugged
      And I run "sudo cp features/fixtures/config/signature_thresholds.yaml /var/rugged/.config/rugged/config.yaml"
      And I run "sudo -u rugged rugged generate-keys"
      And I run "sudo -u rugged rugged initialize --local"
     When I run "sudo -u rugged rugged status --worker=root-worker"
     Then I should get:
          """
          === Repository status for root-worker ===
          Role       Capability    Signatures
          ---------  ------------  ------------
          targets    Signing       1 / 2
          snapshot   Signing       1 / 2
          timestamp  Signing       1 / 2
          root       Signing       2 / 2
          """
      And I should not get:
          """
          === Repository status for timestamp-worker ===
          === Repository status for snapshot-worker ===
          === Repository status for targets-worker ===
          """
     When I run "cat fixtures/tuf_repo/metadata/1.root.json"
     Then I should get:
          """
          "threshold": 2
          """
      And I should not get:
          """
          "threshold": 1
          """

  # This appears to be a false negative. Presumably something should fail when
  # insufficient signatures are present.
  @wip
  Scenario: Roles with insufficient signatures will cause an error.
    Given I reset Rugged
      And I run "sudo cp features/fixtures/config/signature_thresholds.yaml /var/rugged/.config/rugged/config.yaml"
      And I run "sudo -u rugged rugged generate-keys"
      And I run "sudo -u rugged rugged initialize --local"
     When I run "sudo -u rugged rugged status --worker=root-worker"
     Then I should get:
          """
          === Repository status for root-worker ===
          Role       Capability    Signatures
          ---------  ------------  ------------
          targets    Signing       1 / 2
          snapshot   Signing       1 / 2
          timestamp  Signing       1 / 2
          root       Signing       2 / 2
          """
      And I should not get:
          """
          === Repository status for timestamp-worker ===
          === Repository status for snapshot-worker ===
          === Repository status for targets-worker ===
          """
    Given I am in the "/var/rugged/incoming_targets" directory
      And file "test0.txt" contains "test0"
      And I run "sudo -u rugged rugged add-targets"
      And I am in the "/var/www/html" directory
      And I publish TUF metadata
      And I register the Rugged repo with Composer
      And I use the "rugged-composer.json" Composer file
      And I am in the "d9-site" directory
      And I run "composer install -vv"
      And I run "rm -rf web/modules/contrib/token/"
      And I run "composer install -vv"
