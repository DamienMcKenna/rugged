@rugged @pipenv
Feature: Ensure tests are running against latest code.
  In order to test recent changes to the codebase
  As a developer
  I need to ensure that tests are run under pipenv.

  Scenario: Check that `rugged` ie being called from the correct location.
     When I run "which rugged"
     Then I should get:
     """
     /usr/local/bin/rugged
     """
     When I run "sudo -u rugged rugged --version"
     Then I should get:
     """
     rugged, version 0.1.0
     """
     When I run "pipenv run sudo -u rugged rugged --version"
     Then I should get:
     """
     rugged, version 0.1.0
     """
