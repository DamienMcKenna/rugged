@testing @example
Feature: Testing tools
  In order to test
  As a developer
  I need to ensure the proper tools are installed and working

  Scenario: Check that Behat is installed
     When I run "./bin/behat --version"
     Then I should get:
       """
       behat 3.
       """
