@rugged @workers @targets-worker
Feature: A worker to sign TUF repo targets metadata.
  In order to securely sign TUF repo targets metadata
  As an administrator
  I need to dispatch tasks to a 'targets' worker.

  Background:
    Given I reset Rugged

  Scenario: Send a basic ping/echo message.
     When I run "sudo -u rugged rugged echo --worker=targets-worker --timeout=1"
     Then I should get:
          """
          Sending targets-worker Ping!...
          Done. Response was: targets-worker PONG: Ping!
          """

  Scenario: Retrieve worker logs.
    Given I run "sudo -u rugged rugged echo --worker=targets-worker --timeout=1"
     When I run "sudo -u rugged rugged logs --worker=targets-worker"
     Then I should get:
          """
          INFO (base_worker.echo): targets-worker received echo task: Ping!
          """
