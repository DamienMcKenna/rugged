import click
import click_log
import logging
from rugged.commands.add_targets import add_targets_cmd
from rugged.commands.config import config_cmd
from rugged.commands.echo import echo_cmd
from rugged.commands.generate_keys import generate_keys_cmd
from rugged.commands.initialize import initialize_cmd
from rugged.commands.logs import logs_cmd
from rugged.commands.remove_targets import remove_targets_cmd
from rugged.commands.rotate_keys import rotate_keys_cmd
from rugged.commands.status import status_cmd
from rugged.commands.validate import validate_cmd
from rugged.lib.constants import __version__
from rugged.lib.logger import get_rugged_logger
from rugged.lib.user import check_user

check_user()
log = get_rugged_logger()


@click.group()
@click.version_option(version=__version__)
@click_log.simple_verbosity_option(log)
@click.option('--debug', is_flag=True, help='Output and log debug messages.')
def rugged_cli(debug):
    """A CLI tool for the Rugged TUF Server."""
    if debug:
        log.setLevel(logging.DEBUG)
    log.debug("rugged_cli invoked")


def register_subcommands():
    """Add sub-commands to the CLI tool."""
    rugged_cli.add_command(add_targets_cmd)
    rugged_cli.add_command(config_cmd)
    rugged_cli.add_command(echo_cmd)
    rugged_cli.add_command(generate_keys_cmd)
    rugged_cli.add_command(initialize_cmd)
    rugged_cli.add_command(logs_cmd)
    rugged_cli.add_command(remove_targets_cmd)
    rugged_cli.add_command(rotate_keys_cmd)
    rugged_cli.add_command(status_cmd)
    rugged_cli.add_command(validate_cmd)


register_subcommands()
