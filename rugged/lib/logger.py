import click_log
import logging
import sys
from rugged.lib.config import get_config
from rugged.lib.constants import RUGGED_LOGGER


class Log:

    def __init__(self, name):
        self.logger = logging.getLogger(name)
        click_log.basic_config(self.logger)
        config = get_config()
        handler = logging.FileHandler(config['log_file'].get())
        formatter = logging.Formatter(config['log_format'].get())
        handler.setFormatter(formatter)
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(handler)

    def get_logger(self):
        return self.logger


def get_rugged_logger():
    """ Instantiate, configure and return the TUF logging service."""
    return Log(RUGGED_LOGGER).get_logger()


def get_logger():
    """ Return the existing TUF logging service."""
    return logging.getLogger(RUGGED_LOGGER)


def get_log_files(log):
    """ Return a list of log files for the given logger. """
    log_files = []
    for handler in log.handlers:
        if hasattr(handler, 'baseFilename'):
            log_files.append(handler.baseFilename)
    return log_files


def get_log_entries(log):
    """ Return a dictionary of log entries for a given log. """
    log_files = get_log_files(log)
    return _get_log_entries(log_files)


def _get_log_entries(log_files):
    """ Gather a dictionary of log entries. """
    log_entries = {}
    for logfile in log_files:
        with open(logfile, 'r') as file:
            log_entries[logfile] = file.readlines()
    return log_entries


def truncate_log(log):
    """ Delete all entries from logs. """
    truncated_log_files = []
    for logfile in get_log_files(log):
        with open(logfile, 'w') as file:
            file.truncate
        truncated_log_files.append(logfile)
    return truncated_log_files


def log_exception(exception):
    """ Log an exception's class and message. """
    log = get_logger()
    exception_class = exception.__class__.__name__
    caller = sys._getframe().f_back.f_code.co_name
    log.error(f"{exception_class} thrown in {caller}: {exception}")


def set_log_level_from_context(context):
    """ Set the log level based on the context passed to a task. """
    log = get_logger()
    log.setLevel(context['log_level'])
