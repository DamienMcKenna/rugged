from getpass import getuser
import sys

RUGGED_USER = 'rugged'


def check_user():
    if getuser() != RUGGED_USER:
        error = "Rugged can only be run as the 'rugged' user. "\
                "Try 'sudo -u rugged ...'"
        sys.exit(error)
