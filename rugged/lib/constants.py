__version__ = "0.1.0-dev"
RUGGED_LOGGER = 'rugged'

RUGGED_SIGNING_KEY_DIR = '/var/rugged/signing_keys'
RUGGED_VERIFICATION_KEY_DIR = '/var/rugged/verification_keys'

RUGGED_KEY_INDEX_DELIMITER = ':'
