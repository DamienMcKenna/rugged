import click
from rugged.lib.config import get_config

config = get_config()


def print_host_header(message, host):
    if not config['print_host_headers'].get():
        return
    click.echo(f"=== {message} for {host} ===")
