from rugged.lib.task_queue import TaskQueue
from rugged.workers.base_worker import BaseWorker

worker = TaskQueue().get_task_queue()


class TestWorker(BaseWorker):
    """
    Rugged (Celery) worker for testing of base/common functionality.

    N.B. This worker has access to the targets signing key.

    This worker should not be active in production environments. It is only
    intended for development and testing.
    """
