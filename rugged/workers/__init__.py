from rugged.lib.config import get_config

config = get_config()


def get_workers(workers={}):
    """ Return a list of given workers; default to all workers. """
    workers = list(workers)
    # If workers have been specified, then just return that list.
    if workers:
        return workers
    for key, worker in config['workers'].get().items():
        workers.append(worker['name'])
    return workers
