from rugged.lib.logger import get_logger
from securesystemslib.exceptions import StorageError

log = get_logger()


class RuggedStorageError(StorageError):

    def __init__(
        self,
        msg="The repository could not create the needed directories."
    ):
        self.msg = msg
        log.debug(f"{self.__class__.__name__}: {msg}")

    def __str__(self):
        return self.msg
