from kombu.exceptions import OperationalError
from rugged.lib.logger import get_logger

log = get_logger()


class RuggedHostnameNotFound(OperationalError):

    def __init__(self, msg="Failed to resolve RabbitMQ hostname."):
        self.msg = msg
        log.debug(f"{self.__class__.__name__}: {msg}")

    def __str__(self):
        return self.msg
