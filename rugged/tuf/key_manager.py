import os
import shutil
import sys
from pathlib import Path
from rugged.lib.constants import (
    RUGGED_SIGNING_KEY_DIR,
    RUGGED_VERIFICATION_KEY_DIR,
)
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger, log_exception
from securesystemslib.exceptions import StorageError
from securesystemslib.interface import (
    _generate_and_write_ed25519_keypair,
    import_ed25519_publickey_from_file,
    import_ed25519_privatekey_from_file,
)


from tempfile import TemporaryDirectory

config = get_config()
log = get_logger()


class KeyManager():
    """ Provides key CRUD functionality. """

    """ Static cache for keys found on the filesystem. """
    _role_keys = {}

    def generate_keypair(self, key_name, role_name):
        """ Generate a keypair for a given role """
        if not self._ensure_rugged_key_dirs():
            return (False, False)
        with TemporaryDirectory() as tempdir:
            temp_privkey_path = f"{tempdir}/{role_name}/{key_name}"
            temp_pubkey_path = f"{temp_privkey_path}.pub"
            log.debug(f"Generating keypair at {temp_privkey_path}.")
            # @TODO: Add support for passwords.
            _generate_and_write_ed25519_keypair(filepath=temp_privkey_path)
            privkey_result = self._copy_key(temp_privkey_path, key_name, role_name, 'signing')
            pubkey_result = self._copy_key(temp_pubkey_path, key_name, role_name, 'verification')
        # Clear the cache for this role, so that the directory will
        # be re-scanned to pick up the new key.
        self._clear_cache_by_role(role_name)
        return (privkey_result, pubkey_result)

    def _clear_cache_by_role(self, role_name: str):
        """ Remove a role from the key cache, to ensure the role's key directories get re-scanned. """
        if role_name in self._role_keys:
            del self._role_keys[role_name]

    def _ensure_rugged_key_dirs(self):
        """ Ensure the Rugged key directories exist. """
        for key_dir in [RUGGED_SIGNING_KEY_DIR, RUGGED_VERIFICATION_KEY_DIR]:
            try:
                os.makedirs(key_dir, mode=0o700, exist_ok=True)
            except PermissionError as e:
                log_exception(e)
                return False
        return True

    def _copy_key(self, temp_key_path, key_name, role_name, key_type):
        """ Move a key into place, based on role and type. """
        key_path = self.get_key_path(key_name, role_name, key_type)
        log.debug(f"Copying {key_type} key to {key_path}.")
        try:
            shutil.copy(temp_key_path, key_path)
            return key_path
        except FileNotFoundError as e:
            log_exception(e)
            return False
        except PermissionError as e:
            log_exception(e)
            return False

    def find_keys(self):
        """ Find keys for all roles. """
        keys = {}
        for role_name in config['roles'].get().keys():
            role_keys = self.find_keys_for_role(role_name)
            if role_keys:
                keys[role_name] = role_keys
        return keys

    def find_keys_for_role(self, role_name):
        """ Find keys for a specific role. """
        if role_name not in self._role_keys:
            self._role_keys[role_name] = []
            for file in os.listdir(f"{RUGGED_VERIFICATION_KEY_DIR}/{role_name}"):
                try:
                    key_name = Path(file).stem
                    if self.load_verification_key(key_name, role_name):
                        log.debug(f"Found '{key_name}' verification key for '{role_name}' role.")
                        self._role_keys[role_name].append(key_name)
                except Exception:
                    # Any error in loading a key means that it's not a valid
                    # key. This is normal, as the key directories are likely
                    # to have `.gitkeep` files, for example. We just ignore
                    # these.
                    pass
            try:
                files = os.listdir(f"{RUGGED_SIGNING_KEY_DIR}/{role_name}")
            except FileNotFoundError:
                # This is to be expected, since most workers won't have access
                # to all the signing keys. So it's not really an error. As
                # such, let's just return the verification keys.
                return self._role_keys[role_name]
            for file in files:
                key_name = Path(file).stem
                if key_name in self._role_keys[role_name]:
                    continue
                try:
                    if self.load_signing_key(key_name, role_name):
                        log.debug(f"Found '{key_name}' signing key for '{role_name}' role.")
                        self._role_keys[role_name].append(key_name)
                except Exception:
                    pass
        return self._role_keys[role_name]

    def load_keys(self, key_name, role_name):
        """ Load public keys and private keys (if available) from storage. """
        signing_key = self.load_signing_key(key_name, role_name)
        if signing_key:
            log.debug(f"Loaded '{key_name}' signing key for '{role_name}' role.")
            return signing_key
        verification_key = self.load_verification_key(key_name, role_name)
        if verification_key:
            log.debug(f"Loaded '{key_name}' verification key for '{role_name}' role.")
            return verification_key
        log.debug(f"Failed to loaded '{key_name}' keys for '{role_name}' role.")
        return False

    def load_signing_key(self, key_name, role_name):
        """ Load a signing key for a given role. """
        key_path = self.get_key_path(key_name, role_name, 'signing')
        try:
            log.debug(f"Loading signing key for '{role_name}' role from '{key_path}'.")
            key = import_ed25519_privatekey_from_file(key_path)
        except StorageError:
            # This is to be expected. So it's not really an error.
            return False
        return key

    def load_verification_key(self, key_name, role_name):
        """ Load a verification key for a given role. """
        key_path = self.get_key_path(key_name, role_name, 'verification')
        try:
            log.debug(f"Loading verification key for '{role_name}' role from '{key_path}'.")
            key = import_ed25519_publickey_from_file(key_path)
        except StorageError:
            # This is to be expected. So it's not really an error.
            return False
        return key

    def delete_keypair(self, key_name, role_name):
        """ Delete a specific keypair. """
        privkey_result = self._delete_key(key_name, role_name, 'signing')
        pubkey_result = self._delete_key(key_name, role_name, 'verification')
        # Clear the cache for this role, so that the stale key won't
        # be be returned when `find_keys()` and friends are called.
        self._clear_cache_by_role(role_name)
        return (privkey_result, pubkey_result)

    def _delete_key(self, key_name, role_name, key_type):
        """ Delete a specific key. """
        key_path = self.get_key_path(key_name, role_name, key_type)
        log.debug(f"Deleting {key_type} key at {key_path}.")
        try:
            os.remove(key_path)
        except Exception as e:
            log_exception(e)
            log.error(f"Failed to delete file at '{key_path}'. Check log for details.")
            sys.exit(os.EX_IOERR)
        return key_path

    def get_key_path(self, key_name, role_name, key_type):
        """ Return the path of a specific key, based on its name, role and type. """
        if key_type == 'signing':
            key_dir = RUGGED_SIGNING_KEY_DIR
            extension = ''
        elif key_type == 'verification':
            key_dir = RUGGED_VERIFICATION_KEY_DIR
            extension = '.pub'
        else:
            error = f"Unrecognized key type '{key_type}'."
            error += "Key type must be either 'signing' or 'verification'."
            raise Exception(error)
        return f"{key_dir}/{role_name}/{key_name}{extension}"
