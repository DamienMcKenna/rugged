from collections import OrderedDict
from datetime import datetime, timedelta
from glob import glob
from os import chdir, listdir, makedirs, path, remove, rmdir
from re import sub
from shutil import move
from securesystemslib.signer import SSlibSigner
from typing import Any, Dict

from tuf.api.metadata import (
    SPECIFICATION_VERSION,
    Key,
    Metadata,
    MetaFile,
    Root,
    Role,
    Snapshot,
    TargetFile,
    Targets,
    Timestamp,
)
from tuf.api.serialization.json import JSONSerializer

from rugged.exceptions.key_error import RuggedKeyError
from rugged.exceptions.metadata_error import RuggedMetadataError
from rugged.exceptions.repository_error import RuggedRepositoryError
from rugged.exceptions.storage_error import RuggedStorageError
from rugged.lib.config import get_config
from rugged.lib.constants import RUGGED_KEY_INDEX_DELIMITER
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.key_manager import KeyManager

config = get_config()
log = get_logger()
SPEC_VERSION = ".".join(SPECIFICATION_VERSION)


def _in(days: float) -> datetime:
    """Adds 'days' to now and returns datetime object w/o microseconds."""
    return datetime.utcnow().replace(microsecond=0) + timedelta(days=days)


class RuggedRepository():
    """
    An opinionated TUF repository using the low-level TUF Metadata API.

    @TODO: Implement support for hashed bins. (#99)
    @TODO: Implement support for consistent snapshots. (#100)
    @TODO: Implement support for delegated targets. (#36)
    """

    def __init__(self):
        try:
            self._init_dirs()
            self._init_keys()
            self._init_roles()
            log.debug("Instantiated repository.")
        except Exception as e:
            log_exception(e)
            log.error("Failed to instantiate repository.")
            raise RuggedRepositoryError()

    def load(self):
        """ Load all metadata from storage. """
        for role_name in self.roles.keys():
            try:
                self.load_metadata(role_name)
            except RuggedMetadataError:
                error = "Failed to load all metadata."
                log.error(error)
                raise RuggedMetadataError(error)
        log.debug("Loaded all metadata.")

    def load_metadata(self, role_name: str):
        """ load a role's metadata from storage. """
        metadata_file = self._get_metadata_path(role_name)
        try:
            self.roles[role_name] = self.roles[role_name].from_file(metadata_file)
            message = f"Loaded metadata for '{role_name}' role from "\
                      f"'{metadata_file}'."
            log.debug(message)
        except TypeError as e:
            log_exception(e)
            error = f"Failed to load metadata for '{role_name}' role from "\
                    f"'{metadata_file}'."
            raise RuggedMetadataError(error)

    def _get_metadata_path(self, role_name, new=False):
        """ Determine the path for a given role's metadata file. """
        metadata_dir = config['repo_metadata_path'].get()
        filename = self._get_metadata_filename(role_name, new)
        return path.join(metadata_dir, filename)

    def _get_metadata_filename(self, role_name, new=False):
        """ Determine the filename for a given role's metadata. """
        filename = f"{self.roles[role_name].signed.type}.json"
        # Timestamp metadata doesn't use consistent snapshots.
        if role_name == "timestamp":
            return filename
        # We need to special-case root here, since it should always have the
        # version prefix. This is what will allow clients to find and validate
        # the chain of root metadata that links the original (shipped) root to
        # its current version.
        if role_name == 'root' or config['consistent_snapshot'].get():
            if new:
                # Use the version in the role metadata that has already been incremented.
                filename = f"{self.roles[role_name].signed.version}.{filename}"
            else:
                # Otherwise, find the most recent one that exists in storage (eg. on disk).
                filename = self._get_versioned_metadata_filename(filename)
        return filename

    def _get_versioned_metadata_filename(self, filename):
        """ Determine the versioned filename for a given role's metadata. """
        metadata_dir = config['repo_metadata_path'].get()
        chdir(metadata_dir)
        files = glob(f"*.{filename}")
        # Sort numbers naturally. Based on magic from https://stackoverflow.com/a/33159707.
        files.sort(key=lambda f: int(sub(r'\D', '', f)))
        # Return the latest version.
        return files[-1]

    def write(self):
        """ Write all metadata to storage. """
        for role_name in self.roles.keys():
            result = self.write_metadata(role_name)
            if not result:
                return False
        return True

    def write_metadata(self, role_name: str):
        """ Write a role's signed metadata to storage. """
        PRETTY = JSONSerializer(compact=False)
        path = self._get_metadata_path(role_name, new=True)
        try:
            self._sign_metadata(role_name)
            self.roles[role_name].to_file(path, serializer=PRETTY)
        except Exception as e:
            log_exception(e)
            error = f"Failed to write '{role_name}' metadata to file '{path}'."
            log.error(error)
            return False
        log.debug(f"Wrote '{role_name}' metadata to file '{path}'.")
        return True

    def _key_index(self, role_name, key_name):
        """ Return a unique index based on a key's role and name. """
        return f"{role_name}{RUGGED_KEY_INDEX_DELIMITER}{key_name}"

    def _key_name(self, key_index):
        """ Return a key's name based on its index. """
        return key_index.split(RUGGED_KEY_INDEX_DELIMITER)[1]

    def _sign_metadata(self, role_name: str):
        """ Sign a role's metadata. """
        # This needs to use KeyManager because the root role may not be defined yet.
        for key_name in KeyManager().find_keys_for_role(role_name):
            key_index = self._key_index(role_name, key_name)
            key = self.keys[key_index]
            try:
                signer = SSlibSigner(key)
                self.roles[role_name].sign(signer, append=True)
            except Exception as e:
                log_exception(e)
                log.error(f"Failed to sign '{role_name}' metadata with '{key_name}' key.")
            log.debug(f"Signed '{role_name}' metadata with '{key_name}' key.")

    def add_targets(self):
        """ Add any inbound targets to the targets metadata. """
        inbound_targets = self.get_inbound_targets()
        added_targets = []
        if not self.roles["targets"]:
            return added_targets
        for inbound_target in inbound_targets:
            try:
                moved_target_path = self._move_inbound_target_to_targets_dir(
                    inbound_target
                )
                target_file_info = TargetFile.from_file(
                    inbound_target,
                    moved_target_path,
                )
                self.roles["targets"].signed.targets[inbound_target] = target_file_info
                message = f"Added target '{inbound_target}' to the repository."
                log.info(message)
                added_targets.append(inbound_target)
            except Exception as e:
                log_exception(e)
                warning = f"Failed to add target '{inbound_target}' to the repository."
                log.warning(warning)
        if added_targets:
            self.roles["targets"].signed.version += 1
        return added_targets

    def get_inbound_targets(self):
        """ Scan the inbound directory for files to add to the repository. """
        inbound_targets_dir = config['inbound_targets_path'].get()
        message = f"Scanning for inbound targets in '{inbound_targets_dir}'"
        log.debug(message)
        chdir(inbound_targets_dir)
        inbound_targets = []
        for inbound_target in glob('**', recursive=True):
            if path.isdir(inbound_target):
                # We only want files, not intermediate directories.
                continue
            log.debug(f"Found target: {inbound_target}")
            inbound_targets.append(inbound_target)
        return inbound_targets

    def _move_inbound_target_to_targets_dir(self, inbound_target):
        """ Move an inbound target to the repo targets directory. """
        inbound_targets_path = config['inbound_targets_path'].get()
        inbound_target_path = path.join(inbound_targets_path, inbound_target)
        moved_target_path = path.join(
            config['repo_targets_path'].get(),
            inbound_target,
        )
        try:
            makedirs(path.dirname(moved_target_path), exist_ok=True)
            move(inbound_target_path, moved_target_path)
            message = f"Moved '{inbound_target_path}' to "\
                      f"'{moved_target_path}'"
            log.debug(message)
        except Exception as e:
            log_exception(e)
            warning = f"Failed to move target '{inbound_target}' to the "\
                      "targets directory."
            log.warning(warning)
        self._delete_empty_target_dirs(inbound_targets_path, inbound_target)
        message = f"Moved inbound target '{inbound_target}' to targets "\
                  "directory."
        log.info(message)
        return moved_target_path

    def _delete_empty_target_dirs(self, root_dir, target):
        """ Delete any intermediate (empty) directories for a target path. """
        if root_dir not in target:
            target = path.join(root_dir, target)
        target_dir = path.dirname(target)
        if target_dir == root_dir:
            return   # This target is the root directory, so stop.
        if listdir(target_dir):
            return   # We're only cleaning up empty directories.
        try:
            rmdir(target_dir)
            log.debug(f"Cleaned up empty directory '{target_dir}'.")
            # Recurse until we hit the root directory.
            self._delete_empty_target_dirs(root_dir, target_dir)
        except OSError as e:
            log_exception(e)
            warning = f"Failed to clean up empty directory '{target_dir}'."
            log.warning(warning)

    def remove_targets(self, targets):
        """ Remove given targets from the targets metadata. """
        removed_targets = []
        for target in targets:
            try:
                del self.roles["targets"].signed.targets[target]
                log.info(f"Removed target '{target}' from the repository.")
                self._delete_removed_target(target)
                removed_targets.append(target)
            except Exception as e:
                log_exception(e)
                warning = f"Failed to remove target '{target}' from the "\
                          "repository."
                log.warning(warning)
        if removed_targets:
            self.update_targets()
        return removed_targets

    def _delete_removed_target(self, removed_target):
        """ Delete the file for the target that we removed from the repo. """
        repo_targets_path = config['repo_targets_path'].get()
        target_file = path.join(repo_targets_path, removed_target)
        try:
            remove(target_file)
            log.info(f"Deleted target file '{target_file}'.")
        except Exception as e:
            log_exception(e)
            log.warning(f"Failed to delete target file '{target_file}'.")
        self._delete_empty_target_dirs(repo_targets_path, removed_target)

    def update_targets(self):
        """ Update root to account for new targets, or rotated keys. """
        self.roles["targets"].signed.version += 1
        self.roles["targets"].signatures.clear()
        log.info("Updated targets metadata.")

    def update_snapshot(self):
        """ Update snapshot to account for changed targets metadata, or rotated keys. """
        targets_version = self.roles["targets"].signed.version
        self.roles["snapshot"].signed.meta["targets.json"].version = targets_version
        self.roles["snapshot"].signed.version += 1
        self.roles["snapshot"].signatures.clear()
        log.info("Updated snapshot metadata.")

    def update_timestamp(self):
        """ Update timestamp to account for changed snapshot metadata, or rotated keys. """
        snapshot_version = self.roles["snapshot"].signed.version
        self.roles["timestamp"].signed.snapshot_meta.version = snapshot_version
        self.roles["timestamp"].signed.version += 1
        self.roles["timestamp"].signatures.clear()
        log.info("Updated timestamp metadata.")

    def update_root(self):
        """ Update root to account for newly added or removed keys. """
        self.roles["root"].signed.version += 1
        self.roles["root"].signatures.clear()
        log.info("Updated root metadata.")

    def status(self):
        repo_status = {
            'roles': {},
        }
        if 'targets' in self.roles:
            targets = self.roles['targets'].signed.targets
            repo_status['targets'] = {
                'count': len(targets),
                'size': sum(target.length for target in targets.values()),
            }
        for role_name, role_info in self.roles.items():
            repo_status['roles'][role_name] = {
                'signatures': len(role_info.signatures),
                'version': role_info.signed.version,
                'tuf_spec': role_info.signed.spec_version,
                'expires': role_info.signed.expires.timestamp(),
            }
            if 'root' in self.roles:
                threshold = self.roles['root'].signed.roles[role_name].threshold
                repo_status['roles'][role_name]['threshold'] = threshold
            repo_status['roles'][role_name]['keys'] = {}
            for key_index, key in self.keys.items():
                key_name = self._key_name(key_index)
                if key['keyid'] not in role_info.signatures.keys():
                    continue
                key_types = list(key['keyval'].keys())
                if 'private' in key_types:
                    key_path = KeyManager().get_key_path(key_name, role_name, 'signing')
                else:
                    key_path = KeyManager().get_key_path(key_name, role_name, 'verification')
                repo_status['roles'][role_name]['keys'][key_name] = {
                    'types': key_types,
                    'scheme': key['scheme'],
                    'key_path': key_path
                }
        return repo_status

    def get_keys_by_role(self):
        """ Return a dictionary of roles and their associated keys. """
        keys_by_role = {}
        for role_name, role in self.roles.items():
            keys_by_role[role_name] = []
            for keyid in role.signatures:
                for key_index, key in self.keys.items():
                    key_name = self._key_name(key_index)
                    if key['keyid'] == keyid:
                        keys_by_role[role_name].append(key_name)
        return keys_by_role

    def _init_dirs(self):
        """ Ensure all repository directories exist. """
        dirs = {
            config['repo_metadata_path'].get(): 0o755,
            config['repo_targets_path'].get(): 0o755,
        }
        for dir, mode in dirs.items():
            try:
                makedirs(dir, mode=mode, exist_ok=True)
            except PermissionError as e:
                log_exception(e)
                raise RuggedStorageError

    def _init_keys(self):
        """ Initialize a dictionary of keys. """
        self.keys: Dict[str, Dict[str, Any]] = {}
        for role_name, key_names in KeyManager().find_keys().items():
            for key_name in key_names:
                key_index = self._key_index(role_name, key_name)
                self.keys[key_index] = KeyManager().load_keys(key_name, role_name)

    def _init_roles(self):
        """ Initialize a dictionary of roles. """
        self.roles: Dict[str, Metadata] = {}
        self._init_top_level_roles()

    def _init_top_level_roles(self):
        """ Create all top-level metadata objects. """
        self._init_targets_role()
        self._init_snapshot_role()
        self._init_timestamp_role()
        self._init_root_role()

    def _init_targets_role(self):
        """ Create targets metadata object. """
        expiry = config['roles']['targets']['expiry'].get(float)
        log.debug(f"Setting 'targets' metadata expiry to {expiry}.")
        self.roles["targets"] = Metadata[Targets](
            signed=Targets(
                version=1,
                spec_version=SPEC_VERSION,
                expires=_in(expiry),
                targets={},
            ),
            signatures=OrderedDict(),
        )
        log.debug("Initialized 'targets' metadata.")

    def _init_snapshot_role(self):
        """ Create snapshot metadata object. """
        expiry = config['roles']['snapshot']['expiry'].get(float)
        log.debug(f"Setting 'snapshot' metadata expiry to {expiry}.")
        self.roles["snapshot"] = Metadata[Snapshot](
            Snapshot(
                version=1,
                spec_version=SPEC_VERSION,
                expires=_in(expiry),
                meta={"targets.json": MetaFile(version=1)},
            ),
            signatures=OrderedDict(),
        )
        log.debug("Initialized 'snapshot' metadata.")

    def _init_timestamp_role(self):
        """ Create timestamp metadata object. """
        expiry = config['roles']['timestamp']['expiry'].get(float)
        log.debug(f"Setting 'timestamp' metadata expiry to {expiry}.")
        self.roles["timestamp"] = Metadata[Timestamp](
            Timestamp(
                version=1,
                spec_version=SPEC_VERSION,
                expires=_in(expiry),
                snapshot_meta=MetaFile(version=1),
            ),
            signatures=OrderedDict(),
        )
        log.debug("Initialized 'timestamp' metadata.")

    def _init_root_role(self):
        """ Create root metadata object. """
        log.debug("Collecting keys for 'root' metadata.")
        keys = self._get_repo_keys_for_root_role()
        log.debug("Collecting roles for 'root' metadata.")
        roles = self._get_repo_roles_for_root_role()
        expiry = config['roles']['root']['expiry'].get(float)
        log.debug(f"Setting 'root' metadata expiry to {expiry}.")
        self.roles["root"] = Metadata[Root](
            signed=Root(
                version=1,
                spec_version=SPEC_VERSION,
                expires=_in(expiry),
                keys=keys,
                roles=roles,
                consistent_snapshot=False,
            ),
            signatures=OrderedDict(),
        )
        log.debug("Initialized 'root' metadata.")

    def _get_repo_keys_for_root_role(self):
        """ Return all known keys, keyed by ID. """
        repo_keys = {}
        for key in self.keys.values():
            try:
                log.debug(f"Loading key with ID: {key['keyid']}")
                repo_keys[key["keyid"]] = Key.from_securesystemslib_key(key)
            except TypeError as e:
                log_exception(e)
                error = "Failed to generate key metadata during TUF "\
                        "repository initialization."
                raise RuggedMetadataError(error)
        if not repo_keys:
            raise RuggedKeyError
        return repo_keys

    def _get_repo_roles_for_root_role(self):
        """ Return all known roles, keyed by ID. """
        repo_roles = {}
        for role_name, role_info in config['roles'].get().items():
            role_keys = []
            # This needs to use KeyManager because the root role isn't defined yet.
            keys = KeyManager().find_keys()
            if role_name not in keys:
                continue
            for key_name in keys[role_name]:
                key_index = self._key_index(role_name, key_name)
                role_keys.append(self.keys[key_index]["keyid"])
                threshold = role_info['threshold']
                try:
                    log.debug(f"Setting '{role_name}' signature threshold to {threshold}.")
                    repo_roles[role_name] = Role(role_keys, threshold=threshold)
                except ValueError as e:
                    log_exception(e)
                    error = "Failed to generate role metadata during TUF "\
                            "repository initialization."
                    raise RuggedMetadataError(error)
        return repo_roles

    def rotate_keys(self, keys_to_add, keys_to_remove):
        """ Update keys and re-sign metadata. """
        added_keys = self._add_keys(keys_to_add)
        removed_keys = self._remove_keys(keys_to_remove)
        roles_to_regenerate_metadata = []
        for role, key in added_keys + removed_keys:
            if role not in roles_to_regenerate_metadata:
                roles_to_regenerate_metadata.append(role)
        # Rotating a 'targets' key requires regenerating all role metadata.
        if 'targets' in roles_to_regenerate_metadata:
            self.update_root()
            self.write_metadata('root')
            self.update_targets()
            self.write_metadata('targets')
            self.update_snapshot()
            self.write_metadata('snapshot')
            self.update_timestamp()
            self.write_metadata('timestamp')
        # Rotating a 'snapshot' key requires regenerating root, timestamp and snapshot metadata.
        elif 'snapshot' in roles_to_regenerate_metadata:
            self.update_root()
            self.write_metadata('root')
            self.update_snapshot()
            self.write_metadata('snapshot')
            self.update_timestamp()
            self.write_metadata('timestamp')
        # Rotating 'timestamp' key requires regenerating root and timestamp metadata.
        elif 'timestamp' in roles_to_regenerate_metadata:
            self.update_root()
            self.write_metadata('root')
            self.update_timestamp()
            self.write_metadata('timestamp')
        # Rotating 'root' key only requires regenerating root metadata.
        elif 'root' in roles_to_regenerate_metadata:
            self.update_root()
            self.write_metadata('root')
        return (added_keys, removed_keys)

    def _add_keys(self, keys_to_add):
        """ Generate keys and add them to roles in root metadata. """
        added_keys = []
        for role_name, key_name in keys_to_add:
            KeyManager().generate_keypair(key_name, role_name)
            self._add_key_to_keychain(role_name, key_name)
            self._add_key_to_root(role_name, key_name)
            added_keys.append((role_name, key_name))
        return added_keys

    def _add_key_to_keychain(self, role_name, key_name):
        """ Add a key to the keychain. """
        log.debug(f"Adding '{key_name}' key to keychain.")
        key_index = self._key_index(role_name, key_name)
        self.keys[key_index] = KeyManager().load_keys(key_name, role_name)

    def _add_key_to_root(self, role_name, key_name):
        """ Add a key to a role in root metadata. """
        log.debug(f"Adding '{key_name}' key to '{role_name}' role in root metadata.")
        key_index = self._key_index(role_name, key_name)
        key = Key.from_securesystemslib_key(self.keys[key_index])
        self.roles['root'].signed.add_key(role_name, key)

    def _remove_keys(self, keys_to_remove):
        """ Delete keys and remove them from roles in root metadata. """
        removed_keys = []
        for role_name, key_name in keys_to_remove:
            KeyManager().delete_keypair(key_name, role_name)
            self._remove_key_from_root(role_name, key_name)
            self._remove_key_from_keychain(role_name, key_name)
            removed_keys.append((role_name, key_name))
        return removed_keys

    def _remove_key_from_root(self, role_name, key_name):
        """ Remove a key from a role in root metadata. """
        log.debug(f"Removing '{key_name}' key from '{role_name}' role in root metadata.")
        key_index = self._key_index(role_name, key_name)
        keyid = self.keys[key_index]['keyid']
        self.roles['root'].signed.remove_key(role_name, keyid)

    def _remove_key_from_keychain(self, role_name, key_name):
        """ Remove a key from the keychain. """
        log.debug(f"Removing '{key_name}' key from the keychain.")
        key_index = self._key_index(role_name, key_name)
        del self.keys[key_index]
