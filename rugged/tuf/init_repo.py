from rugged.lib.config import get_config
from rugged.exceptions.key_error import RuggedKeyError
from rugged.exceptions.metadata_error import RuggedMetadataError
from rugged.exceptions.repository_error import RuggedRepositoryError
from rugged.exceptions.storage_error import RuggedStorageError
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.repo import RuggedRepository

config = get_config()
log = get_logger()


def init_repo():
    """Initialize a new TUF repository."""
    log.debug("Initializing new TUF repository locally.")
    repo_path = config['repo_path'].get()
    try:
        log.info(f"Initializing new TUF repository at {repo_path}.")
        repo = RuggedRepository()
        result = True
    except RuggedRepositoryError as e:
        log_exception(e)
        result = False
        message = f"Failed to initialize repository at {repo_path}."
    except RuggedStorageError as e:
        log_exception(e)
        result = False
        message = f"Failed to initialize repository at {repo_path}."
    except RuggedKeyError as e:
        log_exception(e)
        result = False
        message = "Failed to load keys during TUF repository "\
                  "initialization."
    if result:
        try:
            result = repo.write()
            message = "TUF repository initialized."
        except RuggedMetadataError as e:
            log_exception(e)
            result = False
            message = "Failed to generate metadata during TUF repository "\
                      "initialization."
    return (result, message)
