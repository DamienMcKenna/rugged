import click
import humanize
import sys
from rugged.exceptions.repository_error import RuggedRepositoryError
from rugged.lib.host_header import print_host_header
from rugged.lib.logger import get_logger, log_exception
from rugged.lib.task_queue import run_task
from rugged.workers import get_workers
from rugged.tuf.repo import RuggedRepository
from tabulate import tabulate
from datetime import datetime

log = get_logger()


@click.command("status")
@click.option(
    "--local",
    is_flag=True,
    default=False,
    help="Print the local repo status. Defaults to true, unless a worker is "
         "specified.",
)
@click.option(
    "--worker",
    default=[],
    multiple=True,
    help="The specific worker from which to retrieve repo status. Can be "
         "passed multiple times.",
)
def status_cmd(local, worker):
    """Print a status message for a TUF repository."""
    failed = False
    if local or not worker:
        try:
            repo = RuggedRepository()
            repo.load()
        except Exception as e:
            log_exception(e)
            log.error("Failed to instantiate repository.")
            sys.exit("Check the logs for more detailed error reporting.")
        _print_status('local operations', repo.status())
    if local and not worker:
        return
    for tuf_worker in get_workers(worker):
        try:
            _print_status(tuf_worker, run_task(tuf_worker, 'status'))
        except RuggedRepositoryError:
            log.error(f"Failed to fetch repository status for {tuf_worker}.")
            failed = True
    if failed:
        sys.exit("Check the logs for more detailed error reporting.")


def _print_status(host, status):
    """ Print formatted status for a given host. """
    print_host_header("Repository status", host)
    _print_targets_info(status)
    _print_role_info(status)
    _print_key_info(status)


def _print_targets_info(status):
    """ Print repo targets information. """
    if 'targets' not in status:
        print("No targets data available.")
        return
    headers = [
        "Targets",
        "Total Size",
    ]
    table = [
        [
            status['targets']['count'],
            humanize.naturalsize(status['targets']['size']),
        ]
    ]
    print(tabulate(table, headers=headers) + "\n")


def _print_role_info(status):
    """ Print table of repo role information. """
    if 'roles' not in status or not status['roles']:
        print("No roles data available.")
        return
    headers = [
        "Role",
        "Capability",
        "Signatures",
        "Version",
        "TUF Spec",
        "Expires",
    ]
    table = []
    for role_name, role_info in status['roles'].items():
        key_type = "Verification"
        for key_info in role_info['keys'].values():
            if 'private' in key_info['types']:
                key_type = "Signing"
                break
        # Round down to nearest minute
        now = datetime.utcnow().replace(second=59)
        expiry = datetime.fromtimestamp(role_info['expires']).replace(second=0)
        signatures = f"{role_info['signatures']}"
        if 'threshold' in role_info:
            signatures += f" / {role_info['threshold']}"
        table.append([
            role_name,
            key_type,
            signatures,
            role_info['version'],
            role_info['tuf_spec'],
            humanize.precisedelta((now - expiry), suppress=['months', 'seconds'], format="%0.0f"),
        ])
    print(tabulate(table, headers=headers) + "\n")


def _print_key_info(status):
    """ Print table of repo key information. """
    if 'roles' not in status or not status['roles']:
        print("No keys data available.")
        return
    headers = [
        "Key name",
        "Role",
        "Key type(s)",
        "Scheme",
        "Path",
    ]
    table = []
    for role_name, role_info in status['roles'].items():
        if 'keys' not in role_info:
            print(f"No keys data available for '{role_name}'.")
            continue
        for key_name, key_info in role_info['keys'].items():
            table.append([
                key_name,
                role_name,
                ", ".join(key_info['types']),
                key_info['scheme'],
                key_info['key_path'],
            ])
    print(tabulate(table, headers=headers) + "\n")
