import click
import sys
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task
from rugged.tuf.init_repo import init_repo

config = get_config()
log = get_logger()
workers = config['workers'].get()


@click.command("initialize")
@click.option(
    "--local",
    is_flag=True,
    default=False,
    help="Generate keys locally, rather than delegating to the root worker.",
)
def initialize_cmd(local):
    """Initialize a new TUF repository."""
    if not local:
        result, message = run_task(workers['root']['name'], 'initialize_task')
    else:
        result, message = init_repo()
    if result:
        log.info(message)
    else:
        log.error(message)
        error = "Failed to initialize TUF repository. "\
                "Check the logs for more detailed error reporting."
        sys.exit(error)
