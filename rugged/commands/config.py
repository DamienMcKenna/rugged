import click
from rugged.lib.config import get_local_configs
from rugged.lib.host_header import print_host_header
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task
from rugged.workers import get_workers
from tabulate import tabulate

log = get_logger()


@click.command("config")
@click.option(
    "--local",
    is_flag=True,
    default=False,
    help="Print the local config. Defaults to true, unless a worker is "
         "specified.",
)
@click.option(
    "--worker",
    default=[],
    multiple=True,
    help="The specific worker from which to retrieve config. Can be passed "
         "multiple times.",
)
def config_cmd(local, worker):
    """ Print the config for a TUF repository. """
    if local or not worker:
        _print_local_configs()
    if local and not worker:
        return
    workers = get_workers(worker)
    _print_worker_configs(workers)


def _print_local_configs():
    """ Print the local config for a TUF repository. """
    configs = get_local_configs()
    _print_configs('local operations', configs)


def _print_worker_configs(workers):
    """ Print the workers' configs for a TUF repository. """
    for worker in workers:
        configs = run_task(worker, 'get_configs')
        _print_configs(worker, configs)


def _print_configs(host, configs):
    """ Print formatted configs for a given host. """
    click.echo(f"=== Configuration for {host} ===")
    print_host_header("Configuration", host)
    print(tabulate(list(configs.items()), tablefmt="plain") + "\n")
