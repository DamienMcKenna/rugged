import click
import sys
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.repo import RuggedRepository

config = get_config()
log = get_logger()


@click.command("rotate-keys")
@click.option('--force', is_flag=True)
def rotate_keys_cmd(force):
    """ Rotate verification and signing keys. """
    log.info("Preparing to rotate keys.")
    repo = _load_repo()
    keys_to_remove = _find_keys_to_remove(repo)
    _confirm_keys_to_remove(keys_to_remove, force)
    keys_to_add = _find_keys_to_add(repo)
    added_keys, removed_keys = repo.rotate_keys(keys_to_add, keys_to_remove)
    for role, key in added_keys:
        log.info(f"Added '{key}' key to the '{role}' role.")
    for role, key in removed_keys:
        log.info(f"Removed '{key}' key from the '{role}' role.")


def _load_repo():
    """ Return the fully loaded TUF repo. """
    try:
        repo = RuggedRepository()
        repo.load()
        return repo
    except Exception as e:
        log_exception(e)
        log.error("Failed to instantiate repository.")
        sys.exit("Check the logs for more detailed error reporting.")


def _find_keys_to_remove(repo: RuggedRepository):
    """ Find any keys currently registered in the TUF repository to remove. """
    keys_to_remove = []
    config_keys = config['keys'].get()
    for role, keys in repo.get_keys_by_role().items():
        if role not in config_keys:
            # @TODO: figure out how to handle a role being removed.
            continue
        for key in keys:
            if key not in config_keys[role]:
                keys_to_remove.append((role, key))
    return keys_to_remove


def _find_keys_to_add(repo: RuggedRepository):
    """ Find any keys not currently registered in the TUF repository to add. """
    keys_to_add = []
    repo_keys = repo.get_keys_by_role()
    for role, keys in config['keys'].get().items():
        if role not in repo_keys:
            # @TODO: figure out how to handle a role being added.
            continue
        for key in keys:
            if key not in repo_keys[role]:
                keys_to_add.append((role, key))
    return keys_to_add


def _confirm_keys_to_remove(keys_to_remove, force):
    """ Prompt for confirmation of any keys to remove. """
    for role, key in keys_to_remove:
        prompt = f"The key '{key}' for the '{role}' role will be removed. Confirm?"
        if force:
            click.echo(prompt + " [y/N]: y")
        else:
            click.confirm(prompt, show_default=True, abort=True)
