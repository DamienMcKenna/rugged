import click
import logging
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task
from rugged.workers import get_workers

log = get_logger()


@click.command("echo")
@click.option(
    '--message',
    '-m',
    default='Ping!',
    help='The message to send to the worker',
)
@click.option(
    '--worker',
    default=[],
    multiple=True,
    help="The specific worker to ping. Can be passed multiple times.",
)
@click.option(
    '--username',
    '-u',
    default=None,
    help='The username for the RabbitMQ connection.',
)
@click.option(
    '--password',
    '-p',
    default=None,
    help='The password for the RabbitMQ connection.',
)
@click.option(
    '--host',
    '-h',
    default=None,
    help='The hostname for the RabbitMQ connection.',
)
@click.option(
    '--timeout',
    '-t',
    default=1,
    help='The time to wait for a task to complete (in seconds)',
)
def echo_cmd(worker, message, timeout, username, password, host):
    """ Ping a worker, by sending an echo task on the queue. """

    workers = get_workers(worker)

    log.level == logging.DEBUG
    for worker in workers:
        log.info(f"Sending {worker} {message}...")
        response = run_task(
            worker,
            'echo',
            [worker, message],
            timeout,
            username,
            password,
            host,
        )
        log.info(f"Done. Response was: {response}")
