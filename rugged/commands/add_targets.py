import click
import sys
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task
from rugged.commands.lib.update_metadata import (
    update_snapshot_metadata,
    update_timestamp_metadata,
)

config = get_config()
log = get_logger()
workers = config['workers'].get()


@click.command("add-targets")
def add_targets_cmd():
    """ Add targets to a TUF repository. """
    result, data = run_task(workers['targets']['name'], 'add_targets_task')
    if result:
        message = "Added the following targets to the repository:\n"
        message += "\n".join(data['added_targets'])
        message += "\nUpdated targets metadata."
        log.info(message)
    else:
        log.error("Failed to add one or more targets to TUF repository.")
        sys.exit("Check the logs for more detailed error reporting.")
    update_snapshot_metadata()
    update_timestamp_metadata()
