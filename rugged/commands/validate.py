import click
import os
import sys
from rugged.exceptions.key_error import RuggedKeyError
from rugged.exceptions.metadata_error import RuggedMetadataError
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger, log_exception
from rugged.tuf.validators.metadata import MetadataValidator
from rugged.tuf.validators.key import KeyValidator

config = get_config()
log = get_logger()


@click.command("validate")
def validate_cmd():
    """ Validate TUF repository metadata, keys and targets. """
    _validate_metadata()
    _validate_keys()


def _validate_metadata():
    """ Validate TUF repository metadata. """
    metadata_validator = MetadataValidator()
    # The order that top-level metadata is loaded is important.
    role_validation_callbacks = {
        'root': 'validate_root',
        'timestamp': 'validate_timestamp',
        'snapshot': 'validate_snapshot',
        'targets': 'validate_targets',
    }
    for role, callback in role_validation_callbacks.items():
        log.debug(f"Calling '{callback}()' to validate {role} metadata.")
        func = getattr(metadata_validator, callback)
        try:
            func()
            log.info(f"Metadata for the '{role}' role is valid.")
        except RuggedMetadataError:
            log.error(f"Metadata for the '{role}' role is not valid.")
            sys.exit(os.EX_DATAERR)


def _validate_keys():
    """ Validate TUF repository keys. """
    key_validator = KeyValidator()
    try:
        key_validator.validate_config_keys()
        log.info("All expected keys are present.")
    except RuggedKeyError as e:
        log_exception(e)
        log.error("An expected key was not found. Check logs for details.")
        sys.exit(os.EX_DATAERR)
    try:
        key_validator.validate_storage_keys()
        log.info("Only expected keys are present.")
    except RuggedKeyError as e:
        log_exception(e)
        log.error("An unexpected key was found. Check logs for details.")
        sys.exit(os.EX_DATAERR)
    try:
        key_validator.validate_repo_keys()
        log.info("All keys in repository are valid.")
    except RuggedKeyError as e:
        log_exception(e)
        log.error("An invalid key was found in the repository. Check logs for details.")
        sys.exit(os.EX_DATAERR)
