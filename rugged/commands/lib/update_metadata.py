import sys
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task

log = get_logger()
config = get_config()
workers = config['workers'].get()


def update_snapshot_metadata():
    """ Dispatch the task to update snapshot metadata. """
    result, message = run_task(workers['snapshot']['name'], 'update_snapshot_task')
    if result:
        log.info(message)
    else:
        log.error(message)
        error = "Failed to update snapshot metadata. "\
                "Check the logs for more detailed error reporting."
        sys.exit(error)


def update_timestamp_metadata():
    """ Dispatch the task to update timestamp metadata. """
    result, message = run_task(workers['timestamp']['name'], 'update_timestamp_task')
    if result:
        log.info(message)
    else:
        log.error(message)
        error = "Failed to update timestamp metadata. "\
                "Check the logs for more detailed error reporting."
        sys.exit(error)
