import click
import os
import sys
from rugged.lib.config import get_config
from rugged.lib.logger import get_logger
from rugged.lib.task_queue import run_task
from rugged.tuf.key_manager import KeyManager
from tabulate import tabulate

config = get_config()
log = get_logger()
workers = config['workers'].get()


@click.command("generate-keys")
@click.option(
    "--role",
    default=[],
    multiple=True,
    help="The specific role for which to generate a keypair. Can be passed "
         "multiple times.",
)
@click.option(
    "--local",
    is_flag=True,
    default=False,
    help="Generate keys locally, rather than delegating to the root worker.",
)
def generate_keys_cmd(role, local):
    """Generate a keypair for verification and signing of TUF metadata."""

    repo_keys = config['keys'].get()
    if role:
        # Validate roles that were passed in as options
        unknown_roles = set(role) - set(repo_keys.keys())
        if unknown_roles:
            error = "Unrecognized role(s):\n"
            error += ", \n".join(unknown_roles)
            log.error(error)
            sys.exit(os.EX_USAGE)
        # Remove any roles not specified as options
        ignored_roles = set(repo_keys.keys()) - set(role)
        for ignored_role in ignored_roles:
            del repo_keys[ignored_role]

    for role_name, key_names in repo_keys.items():
        for key_name in key_names:
            log.info(f"Generating '{key_name}' keypair for '{role_name}' role.")
            if local:
                privkey_path, pubkey_path = KeyManager().generate_keypair(key_name, role_name)
            else:
                privkey_path, pubkey_path = run_task(
                    workers['root']['name'],
                    'generate_keypair_task',
                    [key_name, role_name],
                )
            success = True
            if not privkey_path:
                privkey_path = "Failed to generate signing key."
                success = False
            if not pubkey_path:
                pubkey_path = "Failed to generate verification key."
                success = False
            keypair = [
                ["Signing key:", privkey_path],
                ["Verification key:", pubkey_path],
            ]
            print(tabulate(keypair, tablefmt="plain") + "\n")
            if not success:
                error = "Failed to generate key pair. "\
                        "Check the logs for more detailed error reporting."
                log.error(error)
                sys.exit(os.EX_CANTCREAT)
