# Normalize local development and CI commands.
DDEV = $(shell which ddev)
ifeq ($(DDEV),)
    BEHAT_CMD         = bin/behat
    SATIS_CMD         = satis/bin/satis
    COMPOSER_CMD      = composer
    SITE_COMPOSER_CMD = composer
    BUILD_SATIS_CMD   = cd satis && composer install
    RUGGED_CMD        = sudo -u rugged rugged
    DDEV_EXEC_CMD     =
    DDEV_SUDO_CMD     = sudo
else
    BEHAT_CMD         = ddev behat
    SATIS_CMD         = ddev satis
    COMPOSER_CMD      = ddev composer
    SITE_COMPOSER_CMD = ddev site-composer
    BUILD_SATIS_CMD   = ddev exec --dir=/var/www/html/satis composer install
    RUGGED_CMD        = ddev rugged
    DDEV_EXEC_CMD     = ddev exec
    DDEV_SUDO_CMD     = ddev sudo
endif
behat = $(BEHAT_CMD) --colors
satis = $(SATIS_CMD)
composer = $(COMPOSER_CMD)
site-composer = $(SITE_COMPOSER_CMD)
build-satis = $(BUILD_SATIS_CMD)
rugged = $(RUGGED_CMD)
ddev = $(DDEV)
make = $(MAKE)
make-quiet = $(MAKE-QUIET)
ddev_exec = $(DDEV_EXEC_CMD)
ddev_sudo = $(DDEV_SUDO_CMD)
ddev_sudo_rugged = $(DDEV_SUDO_CMD) sudo -u rugged

# Colour output. See 'help' for example usage.
ECHO       = @echo -e
BOLD       = \033[1m
RESET      = \033[0m
make_color = \033[38;5;$1m  # defined for 1 through 255
GREEN      = $(strip $(call make_color,22))
GREY       = $(strip $(call make_color,241))
RED        = $(strip $(call make_color,124))
WHITE      = $(strip $(call make_color,255))
YELLOW     = $(strip $(call make_color,94))
LIME       = $(strip $(call make_color,10))
LEMON      = $(strip $(call make_color,11))

# Drumkit variables to get specific tool versions
packer_RELEASE = 1.7.6
