.PHONY: tests tests-ci test-steps ci-local

tests: bin/behat lint ##@rugged Run test suite
	@$(behat) --stop-on-failure

#@TODO: Figure out how to install Flake8 properly in CI.
tests-ci: bin/behat ##@rugged Run CI test suite
	@$(behat) --profile=ci

test-steps: bin/behat ##@rugged Print available test step definitions
	@$(behat) -dl

bin/behat:
	@$(composer) install

ci-local: gitlab-runner ##@rugged Run GitLab CI locally
	@gitlab-runner exec docker --docker-privileged tests || true
