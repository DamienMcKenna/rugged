.PHONY: satis reset-satis clean-satis clean-satis-artifacts satis-composer-file satis-repo-vhost

satis: satis/vendor satis-repo-vhost
satis/vendor:
	$(build-satis)

clean-satis: clean-satis-artifacts
	rm -rf satis/vendor
	rm -rf satis/bin

# Ensure the Satis repo can be served by Nginx.
satis-repo-vhost: /etc/nginx/sites-enabled/satis-repo.conf
/etc/nginx/sites-enabled/satis-repo.conf:
	$(ddev_sudo) cp --no-clobber .ddev/nginx_full/satis-repo.conf $@
	# In DDEV container, nginx won't restart with sudo; but sudo is required in CI.
	$(ddev_exec) service nginx reload || sudo service nginx reload
