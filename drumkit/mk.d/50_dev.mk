.PHONY: dev packaging-pipeline $(RUGGED_WORKERS) redeploy clean-upstream fix-upstream

RUGGED_WORKERS = root-worker snapshot-worker targets-worker test-worker timestamp-worker

dev: packaging-pipeline $(RUGGED_WORKERS) redeploy ##@rugged Override the deployed 'Rugged' package with a virtual environment.

packaging-pipeline:
	@$(ECHO) "$(YELLOW)Updating $@.$(RESET)"
	@$(ddev) exec "cd /opt/rugged/; sudo pipenv install --system --dev; pipenv run sudo pip install -e ."

$(RUGGED_WORKERS):
	@$(ECHO) "$(YELLOW)Updating $@.$(RESET)"
	@$(ddev) -s $@ exec "cd /opt/rugged/; sudo pipenv install --system --dev; pipenv run sudo pip install -e ."

redeploy:
	@for worker in $(RUGGED_WORKERS); do \
          echo -e "$(YELLOW)Restarting $$worker.$(RESET)" ; \
          $(ddev) $$worker-deploy; \
        done

clean-upstream:
	rm composer-integration php-tuf
fix-upstream: composer-integration php-tuf
composer-integration:
	ln -s d9-site/vendor/php-tuf/composer-integration .
	cd composer-integration; git remote set-url origin git@gitlab.com:drupal-infrastructure/package-signing/composer-tuf-integration.git
	cd composer-integration; git remote add github git@github.com:ergonlogic/composer-integration.git || /bin/true
php-tuf:
	ln -s d9-site/vendor/php-tuf/php-tuf .
	cd php-tuf; git remote set-url origin git@gitlab.com:drupal-infrastructure/package-signing/php-tuf-client.git
	cd php-tuf; git remote add github git@github.com:ergonlogic/php-tuf.git || /bin/true

lint:
	$(ddev) pipenv run flake8
