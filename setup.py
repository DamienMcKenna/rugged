from setuptools import setup, find_packages

setup(
    name='rugged',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
        'Click-log',
        'Celery',
        'Confuse',
        'Tuf',
    ],
    entry_points={
        'console_scripts': [
            'rugged = rugged.lib.cli:rugged_cli',
        ],
    },
)
