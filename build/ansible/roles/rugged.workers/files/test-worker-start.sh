#!/bin/bash
set -x
set -o errexit nounset pipefail

# @TODO add healthcheck?

# Ensure the mounted TUF repository directory is owned by 'rugged'.
sudo chown rugged:rugged -R /var/rugged/tuf_repo

exec /usr/bin/supervisord -n
